\section{Overview}
\label{Se:Overview}


%In \secref{Experience} we also use SAT solvers to check equivalence between procedures, i.e., determine if two procedures result with isomorphic observable stores.

%% \secref{Restricted} defines a restricted language of formulas \AF\ for expressing program invariants.
%% \secref{AFClosure} then sketches a non-trivial closure property of programs manipulating (potentially cyclic) singly and
%% doubly linked lists --- if the pre-condition, post-condition, and loop invariant are \AF\ expressible
%% then the verification can be expressed using restricted formulas.
%% \secref{Complete} then shows how to simulate these restricted formulas using propositional logic.
%% Finally, \secref{Limitations} discusses the limitations of programming with \AF\ invariants.

\subsection{Programming with Restricted Invariants}
\label{Se:Restricted}

In this paper we require that the specified invariants are \AF\ formulas. That is, they only use reflexive transitive closure but do not explicitly use function symbols and quantifier alternations.
%% Coming up with such formulas may involve some effort from the programmer and cannot always be done.

%In this paper we require that the specified invariants are \AF\ formulas. That is, they only use reflexive transitive closure but do not explicitly use function symbols and quantifier alternations.
%Coming up with such formulas may involve some effort from the programmer and cannot always be done.

\begin{definition}
Let $t_1, t_2, \ldots t_n$ be logical variables or constant symbols.
We define four types of \textbf{atomic propositions}:
%(i)~$\true$,
(i)~$t_1 = t_2$ denoting equality,
(ii)~$r(t_1, t_2, \ldots, t_n)$ denoting the application of relation symbol $r$ of arity $n$,
and
(iii)~$t_1 \B{f^*} t_2$ denoting the existence of $k \geq 0$ such that $f^k(t_1) = t_2$, where
$f^0(t_1) \eqdef t_1$,
and
$f^{k+1}(t_1) \eqdef f (f^k(t_1))$.
We say that $t_1 \B{f^*} t_2$ is a \textbf{reachability constraint} between $t_1$ and $t_2$ via the function $f$.
\textbf{Quantifier-free formulas} (\QF) are Boolean combinations of such formulas without quantifiers.
\textbf{Alternation-free formulas} (\AF) are Boolean combinations of such formulas with additional quantifiers
of the form $\forall^*\!\!:\!\! \varphi$ or  $\exists^*\!\!:\!\! \varphi$ where $\varphi$ is a \QF\ formula.
\textbf{Forall-Exists Formulas} (\FOAE)\ formulas are Boolean combinations of such formulas
with additional quantifiers of the form $\forall^*\exists^*\!\!:\!\! \varphi$ where $\varphi$ is a \QF\ formula.
In particular, $\QF \subset \AF \subset \FOAE$. % \subset \FOReach$.
\end{definition}

\figref{Reverse} presents a Java program for in-situ reversal of a linked list.
Every node of the list has a $\nextf$ field that points to its successor node in the list.
Thus, we can model $\nextf$ as a function that maps a node in the list to its successor.
For simplicity we assume that
the program manipulates the entire heap, that is, the heap consists of just the nodes in the linked list.
To describe the heap that is reachable from the formal parameter $h$, where $h$ points to the head of the input list, we use
the formula
%\begin{equation}
$\forall \alpha: h \B{\nextf^*} \alpha$.
%\label{Eq:whole}
%\end{equation}

We also assume, until \secref{Cyclic}, that the heap  is acyclic, i.e., the formula $ac$ below
is a precondition of $reverse$.
%% Acyclicity uses the \emph{reachability relation}
%% $\nextf^*$ where, as noted above,
%% $\alpha \B{\nextf^*} \beta$ says that node $\beta$ is reachable from
%% node $\alpha$.
%
\begin{equation}
ac \eqdef \forall \alpha, \beta: \alpha \B{\nextf^*} \beta \land \beta \B{\nextf^*} \alpha \implies \alpha = \beta
\label{Eq:acyclic}
\end{equation}

\begin{figure}
\[
\begin{array}{|c|}
\hline
I_0 \eqdef ac\land \forall \alpha: h \B{\nextf^*} \alpha\\
I_3 \eqdef ac \land \forall \alpha, \beta\neq \nullv:
\LIF{d \B{\nextf^*} \alpha}{\alpha \B{\nextf^*}  \beta \impliesBothWays \beta \B{\nextf_0^*} \alpha}
{c \B{\nextf^*} \alpha \land (\alpha \B{\nextf^*} \beta \impliesBothWays  \alpha \B{\nextf_0^*} \beta)}\\
I_9 = ac \land \forall \alpha: d \B{\nextf^*} \alpha \land (\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays  \beta \B{\nextf_0^*} \alpha)\\
\hline
\end{array}
\]
\caption{
\AF\ invariants for $reverse$. 
Note that $\nextf, \nextf_0$ are function symbols while
$\alpha \B{\nextf^*} \beta$, $\alpha \B{\nextf_0^*} \beta$ are atomic propositions on the reachability via directed paths from $\alpha$ to $\beta$
consisting of $\nextf$, $\nextf_0$ edges.}
\label{Ta:ReverseNOF}%
\end{figure}


%\begin{figure}
\begin{wrapfigure}{l}{2.5in}
\vspace{-2em}
\begin{minipage}{2.3in}
\begin{alltt}\small
\begin{tabbing}
No\=de reverse(Node h) \{  \+ \\
0: Node c = h;\\
1: Node d = null;\\
2: wh\=ile  3: (c != null)\+ \{ \\
4: Node t = c.next;\\
5: c.next = null;\\
6: c.next = d;\\
7: d = c;\\
8: c = t;\- \\
\}\\
9: return d; \- \\
\}
\end{tabbing}
\end{alltt}
\vspace{-1em}
\caption{\label{Fi:Reverse}%
A simple Java program that reverses a list in-situ.}
\vspace{-4mm}
\end{minipage}
\end{wrapfigure}
%\end{figure}

\tabref{ReverseNOF} shows the invariants $I_0$, $I_3$ and $I_9$ that describe
a precondition, a loop invariant, and a postcondition of \progreverse.
They are expressed in \AF\ which permits use of function symbols (e.g. $\nextf$) in formulas only to express reachability
 (cf. $\nextf^*$); moreover, quantifier alternation is not permitted.
The notation $\LIF{b}{f}{g}$ is shorthand for the conditional
$(b \land f) \lor (\neg b \land g)$.
% if-then-else formula
% \neil{not sure this is great notation because it's not standard and it has no closing
%   right bracket so it's hard to stick into a formula:  ITE(f,g,h) might be better?}
% \begin{equation}
% \LIF{b}{f}{g} \eqdef(b \land f) \lor (\neg b \land g)
% \label{Eq:LIF}
% \end{equation}




Note that $I_3$ and $I_9$ refer to $\nextf_0$, the value of $\nextf$
at procedure entry.
The postcondition $I_9$ says that $reverse$ preserves
acyclicity of the list and updates $\nextf$
so that, upon procedure termination, the links of the original list have been reversed.
It also says that all the nodes are reachable from $d$ in the reversed list.
$I_3$ says that at loop entry $c$ is non-null and moreover, the original list is partially reversed. That is, any node reachable from $d$ is connected in reverse wrt.~the input list, whereas any node not reachable from $d$ is reachable from $c$ and belongs to the part of the list that has not yet been reversed.
Observe that $I_3$ and $I_9$ only  refer to $\nextf^*$ and never to $\nextf$ alone.
A more natural way to express $I_9$ would be
\begin{equation}
I'_9 \eqdef ac \land \forall \alpha: d \B{\nextf^*} \alpha \land(\forall \alpha, \beta: \nextf(\alpha) = \beta \impliesBothWays \nextf_0(\beta)= \alpha)
\label{Eq:NatSummaryReverse}
\end{equation}
But this formula is not in \AF\ because it explicitly refers to function symbols $\nextf$ and $\nextf_0$
outside a reachability constraint.

\subsection{Inverting Reachability Constraints}
\label{Se:Inverting}

A crucial step in moving from arbitrary {\FOTC} formulas to {\AF} formulas is eliminating explicit uses of functions % and in particular
such as \nextf.
While this may be difficult for a general graph,
we show that this can be done for programs that manipulate (potentially cyclic) singly- and doubly-linked lists.
In this section, we informally demonstrate this elimination for acyclic lists.
We observe that if \nextf\ is acyclic, we can construct $\nextf^+$  from $\nextf^*$
by
\begin{equation}
 \alpha \B{\nextf^+} \beta  \impliesBothWays \alpha \B{\nextf^*} \beta  \land \alpha \neq \beta
\end{equation}
Also, since $\nextf$ is a function, the set of nodes reachable from a node $\alpha$ is totally ordered
by $\nextf^*$.
Therefore, $\nextf(\alpha)$ is the minimal node in this order that is not $\alpha$.
The minimality is expressed using extra universal quantification in
\begin{equation}
 \nextf(\alpha) = \beta  \impliesBothWays \alpha \B{\nextf^+} \beta  \land \forall \gamma: \alpha \B{\nextf^+} \gamma \implies \beta \B{\nextf^*} \gamma
 \label{Eq:Inversion}
\end{equation}

This inversion shows that $\nextf$ can be expressed using \AF\ formulas.
% However, it does not suffice for automatically converting natural invariants into \AF\ formulas because it may introduce alternations.
However, caution must be practiced when using the elimination above, because it may introduce alternations (see~\trappref{nonAF}).
Nevertheless our experiments demonstrate that in a number of commonly occurring examples,
the alternation can be removed or otherwise avoided, yielding an equivalent {\AF} formula.

\subsection{Generating \FOAE\ Verification Conditions}
\label{Se:AFClosure}
Given a program annotated with loop invariants and procedure specifications, it is possible to automatically generate VCs
to check that the invariants are satisfied by all program executions (e.g., see \cite{frade2011verification}).
For example, the VC of \progreverse\ asserts that every
execution which starts in a state satisfying $I_0$
satisfies $I_3$ and that $I_3$ is indeed \textit{inductive}. That is,
if it holds on the loop entry and if the loop is executed, $I_3$ remains true
after the execution.
Finally, the VC asserts that $I_3$ and the negation of the loop condition implies the postcondition $I_9$.

For simplicity, we do not handle deallocation operations here.
Since our logic expresses reachability it does not depend on a particular memory abstraction, and can handle both garbage collection and programs with explicit deallocation.

Unfortunately showing validity of formulas with transitive closure and quantifier alternations, i.e.,
nesting existential inside universal quantifiers or vice versa is very difficult for first-order theorem provers:
existing decision procedures cannot handle such formulas,
because even the simplest use of transitive closure leads to undecidability~\cite{CSL:ImmermanRRSY04}.

In this paper we show that for programs with \AF\ assertions manipulating singly- and doubly-linked lists,
the generated VCs are effectively propositional.
However, \AF\ formulas are not powerful enough to describe the VCs of programs with \AF\ invariants.
The main reason is that the semantics of accessing heap fields, e.g., $x := y.next$ requires one level of alternation.
Therefore, we slightly generalize \AF\ and generate VCs that have the form $\forall^*\exists^*\!\!:\!\! \varphi$ where $\varphi$ is a quantifier-free formula which does not contain
function symbols in terms but may contain reachability and relation symbols.
Validity of formulas in this class, \FOAE, are decidable since their negations have the form $\exists^*\forall^*\!\!:\!\! \varphi$, that is, they belong to the
Bernays-Sch\"{o}nfinkel class of formulas~\cite{JAR:PiskacMB10}.
In fact, the formulas can be checked with a SAT solver by replacing existential quantifiers with distinct Skolem constants, and then grounding all universally quantified
variables by all combinations of constants.
Indeed, Z3 handles these formulas in a precise manner without the need to perform this transformation.

We show that \FOAE\ formulas are closed under weakest preconditions ($\wlp$), i.e.,
for every statement $S$ and postcondition $Q$ expressed as \FOAE\ formula, it
must be the case that $\wlp(S, Q)$ is expressed  as an \FOAE\ formula.
% Here $\wlp(S, Q)$ is the weakest formula which describes the program
% states for which the execution of $S$ leads to states satisfying $Q$.
% \an{The last sentence may be a bit too much hand-holding.}
To show this closure property of \FOAE\ formulas, we rely on recent results in descriptive complexity which prove that for singly-linked data structures
edge mutations are expressible \textit{without}
quantifications~\cite{Phd:Hesse03}.
% Specifically, this means that reachability between fixed list nodes can be updated wrt.~removing and connecting elements, using quantifier-free formulas.
% \ab{Can we rephrase the above sentence as:
%\ms{Noam: this opens problems, but they are not methodically addressed; maybe this has to come later}
Specifically, this means that updates to the reachability relation, wrt.~pointer removals and additions, can be expressed using quantifier-free formulas.
 We note, however, that our applications to program verification go beyond descriptive complexity in several major ways:
%\begin{itemize}
% \item
(i) Programs can create fresh nodes as a result of dynamic allocation statements of the form $x := \NEW$.
% \item
(ii) A heap field read, $x := y.next$, does not mutate the heap but can affect the truth value of reachability constraints.
% \item
(iii) Calls to libraries can mutate the heap in an unbounded way.
% \item
(iv) In order to guarantee correctness of loops and procedures, the
verification is conducted modularly using \AF\ invariants, pre- and postconditions.
For example, to verify the correctness of a code which includes a procedure call, we assert
that the states at the call satisfy the procedure's precondition expressed as an \AF\ formula
and assert that after the call the state satisfies the procedure's  postcondition specified by an \AF\ formula.
%\end{itemize}

\paragraph*{Handling Destructive Updates.}
We first handle the case of statements that assign $\NULL$ to pointer fields and so remove directed paths.
For example, statement $5$ in the {\progreverse} program is modeled by% the weakest precondition for the statement \texttt{c.next = null} is:
\begin{equation}
\wlp(c.next := \NULL, Q) \eqdef
\begin{array}{ll}
& c \neq \nullv\\
\land &
\substitute{Q}{\alpha \B{\nextf^*} \beta}{\alpha \B{\nextf^*} \beta \land (\neg \alpha \B{\nextf^*} c \lor \beta \B{\nextf^*} c)}
\end{array}
\label{Eq:Destructive}
\end{equation}
The assignment removes the outgoing edge from the node pointed to by $c$.
This is a simplified condition that also uses the fact that the manipulated list is acyclic.
An operation of the form $c.\nextf:=\NULL$ deletes an existing path between
nodes $\alpha$ and $\beta$ if the path goes through a (non-null) node $c$.
This situation can be
expressed by the formula $\alpha\B{\nextf^*}c\land\lnot\beta\B{\nextf^*}c$.
So the negation of this formula conjoined with $\alpha\B{\nextf^*}\beta$ must hold in the precondition so that $\alpha\B{\nextf^*}\beta$ holds in the postcondition.
%We currently ignore explicit memory de-allocation.
% For expository purposes, it also requires that $c$ does not point to $\nullv$
% before the statement and thus proves the absence of null-dereferences.
Notice that this rule drastically differs from the standard McCarthy
axiom~\cite{McCarthy62}, which directly assigns a new value to the heap:
\[
\wlp'(c.next := \NULL, Q) \eqdef
\substitute{Q}{\nextf}{\nextf[c \mapsto \nullv]}
\]

We forbid the use of this rule for it uses a function ($\nextf$) and
relies on ``recomputing'' reachability constraints in $Q$ by using the
transitive closure of $\nextf[c \mapsto \nullv]$.  Instead, we
directly update the effect on the reachability relation
$\alpha\B{\nextf^*}\beta$ by substituting it with a quantifier-free
formula shown in (\ref{Eq:Destructive}).  A similar definition exists
for $\wlp$ for statements like $c.\nextf := d$ that add edges, as we
show later in~\tabref{Wprules}.

Surprisingly, the semantics of field dereference statement $t :=
c.\nextf$ is a bit more subtle despite the fact that such a statement
does not modify the heap.  However, a $\wlp$ for field dereference can
also be given in $\FOAE$ (see \secref{WlpAtomic}), thus enabling
verification with a SAT solver in a complete way.

As shown by Hesse~\cite{Phd:Hesse03}, a $\QF$ definition of the effect
on reachability can be also done for cyclic data structures with a
single pointer field.  However, for programs with reachability over
more than one field in general DAGs, quantifiers are
required~\cite{SIGMOD:DS00}.

%\paragraph*{Handling heap traversals.}


\subsection{Decidability of \FOAE}
\label{Se:Decidable}

Reachability constraints written as $\alpha\B{\nextf^*}\beta$ are not
directly expressible in $FOL$.  However, $\FOAE$ formulas can be
reduced to first-order $\forall^*\exists^*$ formulas without function
symbols (which are decidable; see Section~\ref{Se:AFClosure}) in the
following fashion: Introduce a new binary relation symbol $\rtnext$
with the intended meaning that $\rtnext(\alpha, \beta)
\impliesBothWays \alpha \B{\nextf^*} \beta$.  Even though $\rtnext$ is
an uninterpreted relation, we will consistently maintain the fact that
it models reachability.  Every formula $\varphi$ is translated into
\[\varphi'\eqdef\substitute{\varphi}{t_1 \B{\nextf^*} t_2}{\rtnext(t_1, t_2)}\]
For example, the acyclicity relation shown in~(\ref{Eq:acyclic}) is translated into:
\begin{equation}
\widehat{ac} \eqdef \forall \alpha, \beta: \rtnext(\alpha,\beta) \land \rtnext(\beta, \alpha) \implies \alpha = \beta
\label{Eq:acyclicr}
\end{equation}


We add the consistency rule $\Tlinord$ shown in \tabref{Rtnext}, which requires that
$\rtnext$ is a total order.
In \secref{WlpAtomic} and in \trappref{red} we prove that the translated formula $\Tlinord\implies\varphi'$ is valid if and
only if the original formula $\varphi$ is valid.
The proof constructs real models from ``simulated'' $FO$ models using the reachability inversion~(\ref{Eq:Inversion}).
%First-order $\forall^*\exists^*$ formulas without function symbols are decidable as commented in Section~\ref{Se:AFClosure}.
\begin{figure}
\[
\begin{array}{|rcl|}\hline
\Tlinord &\eqdef& \forall \alpha,\beta: \rtnext(\alpha,\beta) \land \rtnext(\beta,
\alpha)\leftrightarrow \alpha = \beta \quad \land\\
&&\forall \alpha, \beta, \gamma: \rtnext(\alpha, \beta) \land \rtnext(\beta, \gamma) \implies
\rtnext(\alpha, \gamma)\quad \land\\
&&\forall \alpha, \beta, \gamma: \rtnext(\alpha, \beta) \land \rtnext(\alpha, \gamma) \implies  (\rtnext(\beta, \gamma) \lor \rtnext(\gamma, \beta))\\
\hline
\end{array}
\]
\caption{\label{Ta:Rtnext}$\Tlinord$ says all points reachable from a given
  point are linearly ordered.}
\end{figure}
%



\subsection{Expressivity of \AF}\label{Se:exp}
Although $\AF$ is a relatively weak logic, it can express interesting
properties of lists.
Typical predicates that express disjointness of two lists and sharing of tails
are expressible in \AF. For example, for two singly-linked lists with headers
$h, k$, $disjoint(h, k) \impliesBothWays \forall \alpha: \alpha\neq\nullv\implies \neg(h \B{\nextf^*} \alpha \land k \B{\nextf^*} \alpha)$.

Another capability still within the power of $\AF$ is to
relax the earlier assumption that the program manipulates the whole memory.
We describe a summary of \progreverse\ on arbitrary acyclic linked lists in a heap that may contain other linked data structures.
Realistic programs obey ownership requirements, e.g., the head $h$ of the list \textit{owns} the input list which means that it is impossible to
reach one of the list nodes without passing through $h$. That is,
\begin{equation}
\forall \alpha, \beta: \alpha\neq\nullv \implies (h \B{\nextf^*} \alpha  \land \beta \B{\nextf^*} \alpha) \implies h \B{\nextf^*} \beta
\label{eq:Owner}
\end{equation}
This requirement is conjoined to the precondition, $ac$, of \progreverse.
Its postcondition is the conjunction of $ac$, the fact that $h_0$ and $d$ reach the same nodes, 
(i.e., $\forall\alpha: h_0 \B{\nextf_0^*}\alpha \impliesBothWays d \B{\nextf^*}\alpha$) and
\begin{equation}
\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays
\LIFFour
{h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}{\beta \B{\nextf_0^*} \alpha\land\beta\neq\nullv}
{\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\alpha \B{\nextf_0^*} \beta}
{h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\false}
{\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}
{\alpha \B{\nextf_0^*} h_0 \land \beta = h_0}
\label{Eq:SreachReverse}
\end{equation}
Here, the bracketed formula should be read as a four-way case, i.e., as disjunction of the formulas
$h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta \land\beta \B{\nextf_0^*} \alpha \land\beta\neq\nullv$;
$\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta \land \alpha \B{\nextf_0^*} \beta$;
$h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta \land \false$;
and,
$\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta \land
\alpha \B{\nextf_0^*} h_0 \land \beta = h_0$.
Intuitively, this summary distinguishes between the following four cases:
(i)~both the source ($\alpha$) and the target ($\beta$)
are in the reversed list
(ii)~both source and target are outside of the reversed list
(iii)~the source is in the reversed list and the target is not,
and
(iv)~the source is outside and the target is in the reversed list.
Cases (i)--(iii) are self-explanatory. For (iv)  reachability can occur when
there exists a path from $\alpha$ to $h_0 = \beta$.
Formula \eqref{Eq:SreachReverse} is in \AF.
In terms of~\cite{POPL:RinetzkyBRSW05}, this means that we assume that the procedure is cutpoint free.
We can also generate an \AF\ summary for a program with \textit{fixed} number of cutpoints,
as is done in \secref{BoundedSharing}.

The general case of unbounded number of cutpoints requires a formula that is outside {\AF}.
A non-\AF\ formula also arises when we want to express
that a program manipulates two lists of equal length; such a formula requires an inductive definition. See \trappref{notexp}
for examples of these formulas.


% \subsection{On the Limitations of \AF}
% \label{Se:Limitations}

% While \AF\ formulas can express interesting invariants and properties of many programs, there are
% of course situations in which requires more expressive invariants.
% %We give one example here and more examples in \secref{ApLimited}

% \paragraph{Ownership}
% So far we have assumed that the \treverse\ procedure manipulates the whole memory.
% It is also possible to describe the effect of the \texttt{reverse} procedure on arbitrary acyclic linked lists and in particular allowing other manipulated
% lists by:
% \begin{equation}
% \begin{array}{l}
% \forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays \\
% \LIFFour
% {h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}{\beta \B{\nextf_0^*} \alpha}
% {\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\alpha \B{\nextf_0^*} \beta}
% {h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\false}
% {\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}
% {\exists \gamma: \alpha \B{\nextf_0^*} \gamma \land (\neg h_0 \B{\nextf_0^*} \gamma)
% \land  \beta \B{\nextf_0^*} \nextf_0(\gamma)}
% \end{array}
% \end{equation}
% % Here,
% % \[
% % \LIFFour{C_1}{T_1}{C_2}{T_2}{C_3}{T_3}{C_4}{T_4}
% % \eqdef
% % (C_1 \land T_1) \lor (C_2 \land T_2) \lor (C_3 \land T_3) \lor (C_4 \land T_4)
% % \]
% %is a four-way case.
% Here, the bracketed formula should be read as a four-way case, i.e., as disjunction of the formulas
% $h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta \land\beta \B{\nextf_0^*} \alpha$;
% $\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta \land \alpha \B{\nextf_0^*} \beta$; and,
% $\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta \land
% \exists \gamma: \alpha \B{\nextf_0^*} \gamma \land (\neg h_0 \B{\nextf_0^*} \gamma) \land  \beta \B{\nextf_0^*} \nextf_0(\gamma)$.
% Intuitively, this summary distinguishes between the following four cases:
% (i)~both the source and the target elements are in the reversed list
% (ii)~both elements are outside of the reversed list
% (iii)~the source element is in the list and the target is not,
% and
% (iv)~the source element is outside the list and the target is.
% This is the tricky case which is handled by quantifying over $\gamma$, the last node which enters the path.
% This introduces alternation of $\exists$ inside $\forall$ and the use of the function symbol $\nextf$ so
% that the formula is outside \FOAE.
% \ab{OK?}

% Fortunately, in our experience realistic programs obey ownership requirements, e.g., in $reverse$, \thead\ \emph{owns} the given list which means that it is impossible to
% reach a node without passing through the head \thead. That is,
% \begin{equation}
% \forall \alpha, \beta: (h \B{\nextf^*} \alpha  \land \beta \B{\nextf^*} \alpha) \implies h \B{\nextf^*} \beta
% \label{eq:Owner}
% \end{equation}
% This requirement becomes a precondition of the reverse procedure.

% In this case, the fourth case in which the source element is outside the list and the target is inside the list, reachabilty can occur when
% there exists a path from $\alpha$ to $h_0 = \beta$, i.e.,
% \begin{equation}
% \forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays
% \LIFFour
% {h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}{\beta \B{\nextf_0^*} \alpha}
% {\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\alpha \B{\nextf_0^*} \beta}
% {h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\false}
% {\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}
% {\alpha \B{\nextf_0^*} h_0 \land \beta = h_0}
% \label{Eq:SreachReverse}
% \end{equation}
% This formula is alternation free without the use of function symbols.
% In terms of~\cite{POPL:RinetzkyBRSW05}, this means that we assume that the procedure is cutpoint free.
% We can also generate \AF\ summary for programs with fixed number of cutpoints.


% \paragraph*{Correlations Between Data Structures}
% Another case which goes outside \AF\ are invariants in programs with correlated lists.
% For example, we cannot express the fact that two programs manipulates two lists of the same length.

%
%\begin{figure}
%\begin{minipage}{1in}
%\begin{alltt}
%\begin{tabbing}
%vo\=id correl_lists(int sz) \{  \+ \\
%Node c = null;\\
%Node d = null;\\
%wh\=ile  (sz > 0)  \+ \{ \\
%Node t = new Node();\\
%t.next = c;\\
%c = t;\\
%t = new Node();\\
%t.next = d;\\
%d = t;\- \\
%\}\\
%wh\=ile (c != null) \{ \+ \\
%c = c.next; \\
%d = d.next;\-\\
%\}\-\\
%\}
%\end{tabbing}
%\end{alltt}
%\end{minipage}
%\caption{\label{Fi:Correl}%
%A simple Java program that creates two correlated lists.}
%\end{figure}
%
%The program in \figref{Correl}  \smallnote{(Deutsch PLDI 94)} demonstrates
%a case where a weak logic is not enough even to prove the absence of null
%dereference in a pointer program. The first {\tt while} loop creates two
%lists of length {\tt sz}. Then, taking advantage of the equal lengths,
%it traverses the first list --- the one pointed to by {\tt c} --- while at
%the same time advances the pointer {\tt d}.
%
%Since each iterator advances one step, the second loop preserves an invariant
%that the lists at {\tt c} and {\tt d} are of the same length. Hence, as long
%as {\tt c} is not $\nullv$, it guarantees that {\tt d} is not $\nullv$ either.
%Unfortunately, such an invariant requires an inductive definition which is
%well outside of \AF:
%%
%\begin{eqnarray*}
%eqlen(x,y) &\eqdef& (x=\nullv \land y=\nullv) \;\;\lor\\
%    && \big(x\neq\nullv \land y\neq\nullv \land eqlen(\nextf(x),\nextf(y))
%        \big)
%\end{eqnarray*}
%
%\begin{comment}
%
%% This example by Deutsch, 1994
%% We may use it in a later passage
%
%\begin{figure}
%\begin{tikzpicture}[listnode/.style={shape=rectangle,draw,minimum size=3mm},
%                    data/.style={shape=rectangle,draw,minimum size=1mm,fill=Grey!40},
%                    >=latex]
%  \foreach \i in {1,2,3,4,5} {
%    \node[listnode](a\i) at (\i-1,0) {};
%    \node[listnode](b\i) at (\i-1,1.2) {};
%    \node[data](d) at (\i-0.9,0.6) {};
%    \draw[->] (a\i) -- (d);
%    \draw[->] (b\i) -- (d);
%  }
%  \draw[->] (a1) -- (a2);
%  \draw[->] (a2) -- (a3);
%  \draw[->] (a3) -- (a4);
%  \draw[->] (a4) -- (a5);
%  \draw[->] (b1) -- (b2);
%  \draw[->] (b2) -- (b3);
%  \draw[->] (b3) -- (b4);
%  \draw[->] (b4) -- (b5);
%  \node at (-0.4,0) {$\scriptstyle k$};
%  \node at (-0.4,1.2) {$\scriptstyle h$};
%\end{tikzpicture}
%\caption{\label{Fi:Deutsch94}Two linked lists with shared data.}
%\end{figure}
%
%Notice, however, that for the correlated linked data structure in \figref{Deutsch94},
%consisting of two linked lists, where each node has a pointer to a data element,
%and all data elements are shared between the two list, keeping the same order ---
%is in fact definable in $\FOAE$ using the formula:
%%
%\renewcommand\epsilon\varepsilon
%\begin{eqnarray*}
% (&\exists &\epsilon: D(h,\epsilon) \land D(k,\epsilon)) \land \\
% &\forall &\alpha,\beta,\gamma,\delta,\epsilon,\zeta:\\
%    && h\B{\nextf^*}\alpha \land \nextf(\alpha)=\beta \land
%       D(\alpha,\gamma) \land \\
%    && k\B{\nextf^*}\delta \land \nextf(\delta)=\epsilon \land
%       D(\delta,\gamma) \implies
%       (D(\beta,\zeta) \impliesBothWays D(\gamma,\zeta))
%\end{eqnarray*}
%
%Where $D$ is a relation representing the data pointer field (note that
%this field has no recursion, so its transitive closure is not needed).
%The subformulas $\nextf(\alpha)=\beta$ and $\nextf(\delta)=\epsilon$
%are definable via a universal $\AF$ formula as was shown in \secref{Inverting}
%\end{comment}
%
%
%
%
%\subsection{Discussion}
%
%The reader may wonder why our simple method suffices for proving interesting properties of linked
%data structures and why we restrict the quantifier prefix of our formulas.
%We observe that program invariants are used both as assumptions and goals of verification and
%therefore both the formula and its negation
%need to be \FOAE\ expressible.
%Empirically we find such alternation free invariants usually exist.
%In contrast the predicate transformer semantics of the program expressed as $\wlp$ is only used in a positive way
%in the right hand side of implication formulas (as shown in  \secref{Prel}),
%thus allowing extra universal quantifications which we need to express the semantics of heap traversals.
%
%\subsection{Experience with SAT Solver}
%
%SAT solvers can verify \FOAE\ formulas in a sound and complete way.
%For example, we can use the $I_9$ invariant to show that $\texttt{reverse}(\texttt{reverse}(h)) = h$ \ms{Ellaborate}.
