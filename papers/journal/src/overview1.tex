\section{Overview}
\label{Se:Overview}

We present a method to verify properties of programs with linked data structures annotated with invariants
in restricted form using SAT solvers.
The method is \emph{accurate}, i.e., given a procedure annotated with assertions describing loop invariants and procedure pre- and post-conditions in a restricted
form, we determine whether
all executions of the procedure satisfy the invariants.
The SAT solver generates a concrete counterexample for every procedure that can violate its invariant.
%In \secref{Experience} we also use SAT solvers to check equivalence between procedures, i.e., determine if two procedures result with isomorphic observable stores.

\secref{Restricted} defines a restricted language of formulas \AF\ for expressing program invariants.
\secref{AFClosure} then sketches a non-trivial closure property of programs manipulating (potentially cyclic) singly and
doubly linked lists --- if the pre-condition, post-condition, and loop invariant are \AF\ expressible
then the verification can be expressed using restricted formulas.
\secref{Complete} then shows how to simulate these restricted formulas using propositional logic.
Finally, \secref{Limitations} discusses the limitations of programming with \AF\ invariants.

\subsection{Programming with Restricted Invariants}
\label{Se:Restricted}

In this paper we require that the specified invariants are \AF\ formulas, that is, they only use reflexive transitive closure but do not explicitly use function symbols and quantifier alternations.
This may involve some effort from the programmer and cannot always be done.

Let $t_1, t_2, \ldots t_n$ be logical variables or constant symbols.
We define three types of \textbf{atomic propositions}:
(i)~$t_1 = t_2$ denoting equality,
(ii)~$r(t_1, t_2, \ldots, t_n)$ denoting the application of relation symbol $r$ of arity $n$,
and
(iii)$t_1 \B{f^*} t_2$ denoting the there exists a $k \geq 0$ such that $f^k(t_1) = t_2$. where
$f^0(t_1) = t_1$,
and
$f^{k+1}(t_1) = f (f^k(t_1))$.
This denotes a \textbf{reachability constraint} between $t_1$ and $t_2$ via the function $f$.
\textbf{Quantifier-free formulas} (denoted by \QF) are Boolean combinations of such formulas without quantifiers.
\textbf{Alternation-free formulas} (denoted by \AF) are Boolean combinations of such formulas with additional non-nested quantifiers.
\textbf{Forall-Exists Formulas} (denoted by \FOAE)\ formulas are Boolean combinations of such formulas
additional additional quantifiers of the form $\forall^*\exists^*: qf$ where $\qf$ is a quantifier-free formula.


\begin{figure}
%\begin{wrapfigure}{l}{2.7in}
%\centering
\begin{minipage}{2.7in}
\begin{alltt}
\begin{tabbing}
No\=de reverse(Node h) \{  \+ \\
0: Node c = h;\\
1: Node d = null;\\
2: wh\=ile  3: (c != null)  \+ \{ \\
4: Node t = c.next;\\
5: c.next = null;\\
6: c.next = d;\\
7: d = c;\\
8: c = t;\- \\
\}\\
9: return d; \- \\
\}
\end{tabbing}
\end{alltt}
\end{minipage}
\caption{\label{Fi:Reverse}%
A simple Java program that reverses a list in-situ.}
%\end{wrapfigure}
\end{figure}


\figref{Reverse} presents a heap-manipulating Java program for in-situ reversal of a linked list.
Every node of the list has a $\nextf$ field that points to its successor node in the list.
Thus, we can model $\nextf$ as a function that maps a node in the list to its successor.
For simplicity we assume, until \secref{Ownership}, that
the program manipulates the entire heap, that is, the heap consists of just the nodes in the linked list.
To describe reachability in the heap that is reachable from the formal parameter $h$, where $h$ points to the head of the input list, we use
the relation
\begin{equation}
r_0 \eqdef \forall \alpha: h \B{\nextf^*} \alpha
\label{Eq:whole}
\end{equation}

We also assume, until \secref{Cyclic}, that the heap  is acyclic, i.e., the formula $ac$
is a precondition of the procedure \footnote{$ac$ could also be expressed as $\neg \alpha \B{\nextf^+} \alpha$.}.
\begin{equation}
ac \eqdef \forall \alpha, \beta: \alpha \B{\nextf^*} \beta \land \beta \B{\nextf^*} \alpha \implies \alpha = \beta
\label{Eq:acyclic}
\end{equation}
To describe acyclicity we use the \emph{reachability relation}
$\nextf^*$ where,
e.g., $\alpha \B{\nextf^*} \beta$ says that node $\beta$ is reachable from
node $\alpha$.

\begin{table}
\[
\begin{array}{|c|}
\hline
I_0 \eqdef ac\\
I_3 \eqdef ac \land c \neq \nullv \land (\forall \alpha, \beta:
\LIF{d \B{\nextf^*} \alpha}{(\alpha \B{\nextf^*}  \beta \impliesBothWays \beta \B{\nextf_0^*} \alpha}
{c \B{\nextf^*} \alpha \land (\alpha \B{\nextf^*} \beta) \impliesBothWays  \alpha \B{\nextf_0^*} \beta)}\\
I_9 = ac \land (\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays  \beta \B{\nextf_0^*} \alpha)\\
\hline
\end{array}
\]
\caption{\label{Ta:ReverseNOF}%
Restricted \AF\ invariants for {\tt reverse} shown in \figref{Reverse}.
Note that $\nextf, \nextf_0$ are function symbols while
$\alpha \B{\nextf^*} \beta$ is atomic proposition on the reachability via directed paths from $\alpha$ to $\beta$
via $\nextf$ edges.}
\end{table}

\tabref{ReverseNOF} shows the invariants $I_0$, $I_3$ and $I_9$ that describe
pre-condition, loop-invariant, and post-condition of \treverse.
They are expressed in \AF\ which permits use of function symbols (e.g. $\nextf$) in formulas only to express reachability
 (cf. $\nextf^*$); neither is quantifier alternation permitted.
The notation $\LIF{b}{f}{g}$ is shorthand for the conditional
$(b \land f) \lor (\neg b \land g)$.
% if-then-else formula
% \neil{not sure this is great notation because it's not standard and it has no closing
%   right bracket so it's hard to stick into a formula:  ITE(f,g,h) might be better?}
% \begin{equation}
% \LIF{b}{f}{g} \eqdef(b \land f) \lor (\neg b \land g)
% \label{Eq:LIF}
% \end{equation}

Note that $I_3$ and $I_9$ refer to $\nextf_0$, the value of $\nextf$
at procedure entry.
The postcondition $I_9$ says that \treverse\ preserves
acyclicity of the list and updates $\nextf$
so that, upon procedure termination, the links of the original list have been reversed.
$I_3$ says that at loop entry $c$ is non-null and moreover, the original list is partially reversed. That is, any node reachable from $d$ is connected in reverse wrt.~the input list whereas any node not reachable from $d$ is reachable from $c$ and belongs to the part of the list that has not yet been reversed.
Observe that $I_3$ and $I_9$ only  refer to $\nextf^*$ and never to $\nextf$ alone.
% For example,
A more natural way to express $I_9$ is
\begin{equation}
I'_9 \eqdef ac \land (\forall \alpha, \beta: \nextf(\alpha) = \beta \impliesBothWays \nextf_0(\beta)= \alpha)
\label{Eq:NatSummaryReverse}
\end{equation}
This formula is not in \AF\ for it explicitly refers to function symbols $\nextf$ and $\nextf_0$
outside a reachability constraint.

\subsection{Inverting Rechability Constraints}
\label{Se:Inverting}

\ms{This section can be shorthand if necessary.}
A crucial step in moving to \AF\ formulas is eliminating explicit usages of functions % and in particular
such as \nextf.
In general this may be difficult for a general graph.
In the paper we show that this can be done for programs which manipulate (potentially cyclic) singly and doubly-linked lists.
In this section, we we informally demonstrate this elimination for acyclic lists.
We observe that since \nextf\ is acyclic, we can construct $\nextf^+$  from $\nextf^*$
by
\begin{equation}
 \alpha \B{\nextf^+} \beta  \impliesBothWays \alpha \B{\nextf^*} \beta  \land \alpha \neq \beta
\end{equation}
Also, since $\nextf$ is a function, the set of nodes reachable from a node $\alpha$ is totally ordered
by $\nextf^*$.
Therefore, $\nextf(\alpha)$ is the minimal node in this order that is not $\alpha$.
The minimality is expressed using extra universal quantification in
\begin{equation}
 \nextf(\alpha) = \beta  \impliesBothWays \alpha \B{\nextf^+} \beta  \land \forall \gamma: \alpha \B{\nextf^+} \gamma \implies \beta \B{\nextf^*} \gamma
 \label{eq:invertion}
\end{equation}

This inversion shows that $\nextf$ can be expressed using \AF\ formulas.
% However, it does not suffice for automatically converting natural invariants into \AF\ formulas because it may introduce alternations.
However, the elimination above does not always suffice to convert a formula into \AF, because it may introduce alternations.
For example, when converting $I'_9$ (from \equref{NatSummaryReverse}),
we obtain % a formula ---
\begin{align*}
\forall\alpha,\beta: &
   \big(\alpha\B{\nextf^+}\beta \land \forall \gamma:
    \alpha\B{\nextf^+}\gamma\implies\beta\B{\nextf^*}\gamma\big)
    \impliesBothWays \\
  &\big(\alpha\B{\nextf^+_0}\beta \land \forall \gamma:
    \alpha\B{\nextf^+_0}\gamma\implies\beta\B{\nextf^*_0}\gamma\big)
\end{align*}
which, when converted to Prenex normal form yields the \textit{non-}\AF\ formula
\begin{eqnarray*}
\forall\alpha,\beta:
   \exists\gamma_1 \forall\gamma'_1&\exists\gamma_2\forall\gamma'_2:&
     \big(
     \alpha\B{\nextf^+}\beta \land
     (\alpha\B{\nextf^+}\gamma_1\implies\beta\B{\nextf^*}\gamma_1)
        \implies\\
    &&\;\alpha\B{\nextf^+_0}\beta \land
      (\alpha\B{\nextf^+_0}\gamma'_1\implies\beta\B{\nextf^*_0}\gamma'_1)
    \big)\\
    &\land\;&
    \big(
     \alpha\B{\nextf^+_0}\beta \land
     (\alpha\B{\nextf^+_0}\gamma_2\implies\beta\B{\nextf^*_0}\gamma_2)
        \implies\\
    &&\;\alpha\B{\nextf^+}\beta \land
      (\alpha\B{\nextf^+}\gamma'_2\implies\beta\B{\nextf^*}\gamma'_2)
    \big)
\end{eqnarray*}
%which is not \AF.

\subsection{Generating \FOAE\ Verification Conditions}
\label{Se:AFClosure}
Given a program annotated with loop invariants and procedure specifications it is possible to automatically generate
verification conditions (VCs) which check that the invariants are satisfied by all program executions (e.g., see \cite{POPL:FlanaganS01}).
For example, the VC of $reverse$ asserts that every
execution of it which starts in a state satisfying $I_0$
satisfies $I_3$ and that $I_3$ is indeed \emph{inductive}. That is,
if it holds on the loop entry and if the loop is executed, $I_3$ remains true
after the execution.
Finally, the VC asserts that $I_3$ and the negation of the loop condition implies the postcondition $I_9$.

Unfortunately showing validity of formulas with transitive closure and quantifier alternations, i.e.,
nesting existential inside universal quantifiers or vice versa is very difficult for first-order theorem provers:
existing decision procedures cannot handle such formulas.
Indeed even the simplest usage of transitive closure leads to undecidability~\cite{CSL:ImmermanRRSY04}.

In this paper we show that for programs with \AF\ assertions manipulating singly- and doubly-linked lists,
the generated VCs are essentially propositional.
Unfortunately, \AF\ formulas are not powerful enough to describe the VCs even for programs with \AF\ invariants.
The main reason is that the semantics of accessing heap fields, e.g., $x := y.next$ requires one level of alternation.
Therefore, we slightly generalize \AF\ and generate VCs that have the form $\forall^*\exists^* \varphi$ where $\varphi$ is a quantifier-free formula which does not contain
function symbols but may contain relation symbols.
Formulas in this class, which we term \FOAE\ in the sequel, are decidable since their negations have the form $\exists^*: \forall^*: \varphi$, that is, they belong to the
Bernays-Sch\"{o}nfinkel class of formulas~\cite{JAR:PiskacMB10}.
In fact, the formulas can be checked with a SAT solver by replacing existential quantifiers with distinct Skolem constants, and then grounding all universally quantified
variables by all combinations of constants.
Indeed, Z3 handles these formulas in a precise manner without the need to perform this transformation.

The main idea is to show that \FOAE\ formulas are closed under weakest preconditions ($\wlp$), i.e.,
for every statement $S$ and postcondition $Q$ expressed as \FOAE\ formula, it
must be the case that $\wlp(S, Q)$ is expressed  as an \FOAE\ formula.
% Here $\wlp(S, Q)$ is the weakest formula which describes the program
% states for which the execution of $S$ leads to states satisfying $Q$.
% \an{The last sentence may be a bit too much hand-holding.}
To show this closure property of \FOAE\ formulas, we rely on recent results in descriptive complexity which prove that for (potentially cyclic) singly-linked data structures
edge mutations are expressible \textit{without}
quantifications~\cite{Phd:Hesse03}.
% Specifically, this means that reachability between fixed list nodes can be updated wrt.~removing and connecting elements, using quantifier-free formulas.
% \ab{Can we rephrase the above sentence as:
Specifically, this means that updates to the reachability relation, wrt.~pointer removals and additions, can be expressed using quantifier-free formulas.
 We note, however, that the program verification problem goes beyond descriptive complexity in several major ways:
%\begin{itemize}
% \item
(i) The program can create fresh nodes as a result of dynamic allocation statements of the form \textit{x := new()}.
% \item
(ii) A heap field read, \textit{x := y.next}, does not mutate the heap but can affect reachability.
% \item
(iii) Calls to libraries can mutate the heap in an unbounded way.
% \item
(iv) In order to guarantee correctness of loops and procedures, the
verification is conducted modularly by assuming that the input state
is an arbitrary state satisfying the invariant. \ab{Make this more precise?}
%\end{itemize}

\subsubsection{Handling Destructive Updates}

We first handle the case of statements which assigns \tnull\ to pointer fields and remove directed paths.
For example, statement $5$ in the reverse program is modeled by% the weakest precondition for the statement \texttt{c.next = null} is:
\begin{equation}
\wlp(c.next := \nullv, Q) \eqdef
\begin{array}{ll}
& c \neq \nullv\\
\land &
\substitute{Q}{\alpha \B{\nextf^*} \beta}{\alpha \B{\nextf^*} \beta \land (\neg \alpha \B{\nextf^*} c \lor \beta \B{\nextf^*} c)}
\end{array}
\label{Eq:Destructive}
\end{equation}
This is a simplified condition that also uses the fact that the manipulated list is acyclic. \ab{The $\beta \B{\nextf^*} c$ is needed to disallow the case
where, in the pre-state, $c$ is in between $\alpha$ and $\beta$ and $c$ is reachable from $\alpha$. Acyclicity is additionally needed to prevent the case where $c$ is reachable from $\beta$ due to a cycle, because then the assignment will break the cycle and $\beta$ will no longer be reachable from $\alpha$ in the post state. Is this correct?}
For expository purposes, it also requires that \tc\ does not point to \tnull before the statement and thus proves the absence of \texttt{null}-dereferences.
The assignment removes the outgoing edge from the node pointed-to by \texttt{c}.
Notice that this rule drastically differs from the standard McCarthy
axiom~\cite{McCarthy62} which directly assigns to the heap
\[
\wlp'(c.next := \nullv, Q) \eqdef
\substitute{Q}{\nextf}{\nextf[c \mapsto \nullv]}
\]
We forbid the usage of the McCarthy rule which uses a function and relies on ``recomputing'' reachability using transitive closure.
Instead, we directly update the effect on reachability using a quantifier-free formula.
This allows us to show that verification can be done with SAT solver in precise way.

As shown in \cite{Phd:Hesse03} this can be also done for cyclic data structures assuming that we are only interested in reachability
over a single field without the use of quantifiers at all.
However, for programs with reachability over more than one field manipulating DAGs it to requires quantifiers~\cite{Book:Immerman99}.

When adding edges via the operation \texttt{c.next = d} we always ensure that immediately before, \texttt{c.next == null}.
The semantics is correct for arbitrary edge additions even in graphs with arbitrary number of pointer selectors and even
when the edges are non-deterministic~\cite{Book:Immerman99}. Moreover, we can also check acyclicity of the mutated list.
For example, statement $6$ in the reverse program is modeled by:
\begin{equation}
\wlp(c.next := d, Q) \eqdef
\begin{array}{ll}
& c \neq \nullv\\
\land & \neg d \B{\nextf^*} c\\
\land &
\substitute{Q}{\alpha \B{\nextf^*} \beta}{\alpha \B{\nextf^*} \beta \lor (\alpha\B{\nextf^*} c \land d \B{\nextf^*} \beta)}
\end{array}
\end{equation}
This statement adds an edge from the list element pointed-to by \texttt{c} to the list element pointed to by \texttt{d}. \ab{Cycles are prevented owing to
$\neg d \B{\nextf^*} c$.}
Again contrast this with the McCarthy axiom which directly assigns to $\nextf$ and requires inferring reachability properties.

\subsubsection{Pointer Traversals}

Surprisingly, the semantics of statements that traverse the heap such as \texttt{t := c.next} is a bit more subtle despite the fact
that it does not modify the heap at all.
The reason is such statements can effect the reachability between many sources.
For example, the loop invariant $I_3$ in the reverse program can be affected by assignments to \texttt{d} since the
invariant quantifies over paths from \texttt{d}.
This statement would normally not be considered by the dynamic complexity community since
it does not modify the store.
Another tricky statement is memory allocation.

In order to handle heap traversal we utilize the fact that $\nextf$ is expressible in terms of reachability and existential quantifications.
Interestingly, we show in \secref{WlpAtomic}, that this can be done by introducing extra universal quantifications.
We also show that the result is an \FOAE\ formula.


\subsection{Simulating Reachability Constraints}

The last step in the translation is introducing a new binary relation symbol $\rtnext$ such that $\rtnext(\alpha, \beta) \impliesBothWays \alpha \B{\nextf^*} \beta$.
Thus even though $\rtnext$ is an uninterpreted relation, we will incrementally maintain the fact
that it models reachability.
Every formula $\varphi$ is translated into $\substitute{\varphi}{t_1 \B{\nextf^*} t_2}{\rtnext(t_1, t_2)}$.
For example, the acyclicity relation shown in \equref{acyclic} is translated into:
\begin{equation}
\widehat{ac} \eqdef \forall \alpha, \beta: \rtnext(\alpha,\beta) \land \rtnext(\beta, \alpha) \implies \alpha = \beta
\label{Eq:acyclicr}
\end{equation}


%Notice that since the invariants in  \tabref{ReverseNOF} do not use function symbols and alternation, their translation into an alternation free formula
%with reachability is straightforward.


We add the consistency rule $\Gamma_{\mbox{\small linOrd}}$ shown in \tabref{Rtnext} which requires that
$\rtnext$ is a total order.
In \secref{Prel} we prove that the translated formulas are implied by these consistency rules if and
only if the original verification formulas are valid.
The proof constructs real models from the simulated models using the reachability inversions.
\begin{table}
\[
\begin{array}{|rcl|}\hline
\Gamma_{\mbox{\small linOrd}} &\eqdef& \forall \alpha,\beta: \rtnext(\alpha,\beta) \land \rtnext(\beta,
\alpha)\leftrightarrow \alpha = \beta \quad \land\\
&&\forall \alpha, \beta, \gamma: \rtnext(\alpha, \beta) \land \rtnext(\beta, \gamma) \implies
\rtnext(\alpha, \gamma)\quad \land\\
&&\forall \alpha, \beta, \gamma: \rtnext(\alpha, \beta) \land \rtnext(\alpha, \gamma) \implies  (\rtnext(\beta, \gamma) \lor \rtnext(\gamma, \beta))\\
\hline
\end{array}
\]
\caption{\label{Ta:Rtnext}$\Gamma_{\mbox{\small linOrd}}$ says all points reachable from a given
  point are linearly ordered.}
\end{table}








\subsection{On the Limitations of \AF}
\label{Se:Limitations}

While \AF\ formulas can express interesting invariants and properties of many programs, there are
of course situations in which requires more expressive invariants.
%We give one example here and more examples in \secref{ApLimited}

\paragraph{Ownership}
So far we have assumed that the \treverse\ procedure manipulates the whole memory.
It is also possible to describe the effect of the \texttt{reverse} procedure on arbitrary acyclic linked lists and in particular allowing other manipulated
lists by:
\begin{equation}
\begin{array}{l}
\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays \\
\LIFFour
{h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}{\beta \B{\nextf_0^*} \alpha}
{\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\alpha \B{\nextf_0^*} \beta}
{h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\false}
{\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}
{\exists \gamma: \alpha \B{\nextf_0^*} \gamma \land (\neg h_0 \B{\nextf_0^*} \gamma)
\land  \beta \B{\nextf_0^*} \nextf_0(\gamma)}
\end{array}
\end{equation}

Here,
\[
\LIFFour{C_1}{T_1}{C_2}{T_2}{C_3}{T_3}{C_4}{T_4}
\eqdef
(C_1 \land T_1) \lor (C_2 \land T_2) \lor (C_3 \land T_3) \lor (C_4 \land T_4)
\]
is a four-way case.
Intuitively, this summary distinguishes between the following four cases:
(i)~both the source and the target elements are in the reversed list
(ii)~both elements are outside of the reversed list
(iii)~the source element is in the list and the target is not,
and
(iv)~the source element is outside the list and the target is.
This is the tricky case which is handled by quantifying over $\gamma$, the last node which enters the path.
This introduces alternation of $\exists$ inside $\forall$ and the usage of the function symbol $\nextf$ so
that the formula is outside \FOAE.
\ab{OK?}

Fortunately, in our experience realistic programs obey ownership requirements, e.g., in $reverse$, \thead\ \emph{owns} the given list which means that it is impossible to
reach a node without passing through the head \thead. That is,
\begin{equation}
\forall \alpha, \beta: (h \B{\nextf^*} \alpha  \land \beta \B{\nextf^*} \alpha) \implies h \B{\nextf^*} \beta
\label{eq:Owner}
\end{equation}
This requirement becomes a precondition of the reverse procedure.

In this case, the fourth case in which the source element is outside the list and the target is inside the list, reachabilty can occur when
there exists a path from $\alpha$ to $h_0 = \beta$, i.e.,
\begin{equation}
\forall \alpha, \beta: \alpha \B{\nextf^*} \beta \impliesBothWays
\LIFFour
{h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}{\beta \B{\nextf_0^*} \alpha}
{\neg h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\alpha \B{\nextf_0^*} \beta}
{h_0 \B{\nextf_0^*} \alpha \land \neg h_0 \B{\nextf_0^*} \beta}{\false}
{\neg h_0 \B{\nextf_0^*} \alpha \land h_0 \B{\nextf_0^*} \beta}
{\alpha \B{\nextf_0^*} h_0 \land \beta = h_0}
\label{Eq:SreachReverse}
\end{equation}
This formula is alternation free without the usage of function symbols.
In terms of~\cite{POPL:RinetzkyBRSW05}, this means that we assume that the procedure is cutpoint free.
We can also generate \AF\ summary for programs with fixed number of cutpoints.


\paragraph{Correlations Between Data Structures}


Another case which goes outside \AF\ are invariants in programs with correlated lists.
For example, we cannot express the fact that two programs manipulates two lists of the same length.

%
%\begin{figure}
%\begin{minipage}{1in}
%\begin{alltt}
%\begin{tabbing}
%vo\=id correl_lists(int sz) \{  \+ \\
%Node c = null;\\
%Node d = null;\\
%wh\=ile  (sz > 0)  \+ \{ \\
%Node t = new Node();\\
%t.next = c;\\
%c = t;\\
%t = new Node();\\
%t.next = d;\\
%d = t;\- \\
%\}\\
%wh\=ile (c != null) \{ \+ \\
%c = c.next; \\
%d = d.next;\-\\
%\}\-\\
%\}
%\end{tabbing}
%\end{alltt}
%\end{minipage}
%\caption{\label{Fi:Correl}%
%A simple Java program that creates two correlated lists.}
%\end{figure}
%
%The program in \figref{Correl}  \smallnote{(Deutsch PLDI 94)} demonstrates
%a case where a weak logic is not enough even to prove the absence of null
%dereference in a pointer program. The first {\tt while} loop creates two
%lists of length {\tt sz}. Then, taking advantage of the equal lengths,
%it traverses the first list --- the one pointed to by {\tt c} --- while at
%the same time advances the pointer {\tt d}.
%
%Since each iterator advances one step, the second loop preserves an invariant
%that the lists at {\tt c} and {\tt d} are of the same length. Hence, as long
%as {\tt c} is not $\nullv$, it guarantees that {\tt d} is not $\nullv$ either.
%Unfortunately, such an invariant requires an inductive definition which is
%well outside of \AF:
%%
%\begin{eqnarray*}
%eqlen(x,y) &\eqdef& (x=\nullv \land y=\nullv) \;\;\lor\\
%    && \big(x\neq\nullv \land y\neq\nullv \land eqlen(\nextf(x),\nextf(y))
%        \big)
%\end{eqnarray*}
%
%\begin{comment}
%
%% This example by Deutsch, 1994
%% We may use it in a later passage
%
%\begin{figure}
%\begin{tikzpicture}[listnode/.style={shape=rectangle,draw,minimum size=3mm},
%                    data/.style={shape=rectangle,draw,minimum size=1mm,fill=Grey!40},
%                    >=latex]
%  \foreach \i in {1,2,3,4,5} {
%    \node[listnode](a\i) at (\i-1,0) {};
%    \node[listnode](b\i) at (\i-1,1.2) {};
%    \node[data](d) at (\i-0.9,0.6) {};
%    \draw[->] (a\i) -- (d);
%    \draw[->] (b\i) -- (d);
%  }
%  \draw[->] (a1) -- (a2);
%  \draw[->] (a2) -- (a3);
%  \draw[->] (a3) -- (a4);
%  \draw[->] (a4) -- (a5);
%  \draw[->] (b1) -- (b2);
%  \draw[->] (b2) -- (b3);
%  \draw[->] (b3) -- (b4);
%  \draw[->] (b4) -- (b5);
%  \node at (-0.4,0) {$\scriptstyle k$};
%  \node at (-0.4,1.2) {$\scriptstyle h$};
%\end{tikzpicture}
%\caption{\label{Fi:Deutsch94}Two linked lists with shared data.}
%\end{figure}
%
%Notice, however, that for the correlated linked data structure in \figref{Deutsch94},
%consisting of two linked lists, where each node has a pointer to a data element,
%and all data elements are shared between the two list, keeping the same order ---
%is in fact definable in $\FOAE$ using the formula:
%%
%\renewcommand\epsilon\varepsilon
%\begin{eqnarray*}
% (&\exists &\epsilon: D(h,\epsilon) \land D(k,\epsilon)) \land \\
% &\forall &\alpha,\beta,\gamma,\delta,\epsilon,\zeta:\\
%    && h\B{\nextf^*}\alpha \land \nextf(\alpha)=\beta \land
%       D(\alpha,\gamma) \land \\
%    && k\B{\nextf^*}\delta \land \nextf(\delta)=\epsilon \land
%       D(\delta,\gamma) \implies
%       (D(\beta,\zeta) \impliesBothWays D(\gamma,\zeta))
%\end{eqnarray*}
%
%Where $D$ is a relation representing the data pointer field (note that
%this field has no recursion, so its transitive closure is not needed).
%The subformulas $\nextf(\alpha)=\beta$ and $\nextf(\delta)=\epsilon$
%are definable via a universal $\AF$ formula as was shown in \secref{Inverting}
%\end{comment}
%
%
%
%
%\subsection{Discussion}
%
%The reader may wonder why our simple method suffices for proving interesting properties of linked
%data structures and why we restrict the quantifier prefix of our formulas.
%We observe that program invariants are used both as assumptions and goals of verification and
%therefore both the formula and its negation
%need to be \FOAE\ expressible.
%Empirically we find such alternation free invariants usually exist.
%In contrast the predicate transformer semantics of the program expressed as $\wlp$ is only used in a positive way
%in the right hand side of implication formulas (as shown in  \secref{Prel}),
%thus allowing extra universal quantifications which we need to express the semantics of heap traversals.
%
%\subsection{Experience with SAT Solver}
%
%SAT solvers can verify \FOAE\ formulas in a sound and complete way.
%For example, we can use the $I_9$ invariant to show that $\texttt{reverse}(\texttt{reverse}(h)) = h$ \ms{Ellaborate}.
