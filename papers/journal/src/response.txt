We thank the reviewers for the their comments.

Our paper shows how to reason about many linked list manipulations in a *simple* way using SAT solvers, instead of developing the most powerful decidable logic. As a benefit, costs are also lower. 

Indeed our paper provides the first complete solution using polynomial space in contrast for example to MONA, STRAND, and LRP which use non-elementary decision procedures.

Also, unlike existing works, the decidability proof is rather easy (Theorem 1 in the appendix).
The more complex part of the paper uses descriptive complexity to show the closure of AFR loop invariants under *all* linked list statements. The essence of the paper lies in the simplicity of the logic and VCs.

Answers to Referee 1

1: "c != null" is a typo; should not be included in I_3. We thank the reviewer for pointing it out.
2: "c.next := null" is not essential but simplifies the wp formulas, as Section 3 explains. We can also assume the compiler automatically adds such statements before field assignments. 
5: Pointers to the end of the lists are indeed common but they are allowed by  the ownership-by-domination (formula 7 in Section 2.5) because the nodes pointed-to by these pointers are dominated by the head. We also note that ownership as domination allows ownership transfer as demonstrated by the reverse example.
7: Correct, we want to also check for the absence of null dereference, so the conjunct x!=null has to be added there.

Our experience indicates that lists of cyclic lists are only used in certain device drivers. Nevertheless, we considered it and coded such an example by breaking the cycles by the programmer as suggested in [12]. The example is available on our web site . We will update the explanation of the cyclic case. 


Answers to Referee 2
After the submission, we successfully applied the tool to verify the correctness of union-find algorithm which includes unbounded sharing.


Answers to Referee 3
[12] uses a more powerful logic with numeric operations and and arbitrary quantifications. We focused on using a simple logic without numeric operations or quantifier alternations. This makes the task of showing closure under WP more challenging for us for heap traversals x := y.next and heap allocations. [12] and [9] only handle destructive updates (x.next := y). Indeed,  even if the invariants are specified using quantifier-free formulas, for programs which traverse lists,  [12] will yield VCs with nested quantifiers whereas our method will not.

[14] is building on MONA, via a non-elementary decision procedure. In contrast, we reduce to EPR which is simpler in theory and in practice and was already used in many contexts as pointed out by the second reviewer. 
Our method is also very simple for programmers to use since it allows an arbitrary number of relations which need not be parameters as in STRAND.

Contrast the simple proof of Theorem 1 in the appendix with the proofs of [14]. The complex part for us was to show closure under VCs. 
