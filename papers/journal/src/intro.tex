\section{Introduction}
\label{Se:Intro}


%\begin{figure}
\begin{wrapfigure}{L}{0.5\textwidth}
\centering
\begin{minipage}[b]{0.4in}
\xymatrix@C50pt{
  *++[F]{state}\ar[r]^{\textrm{database-update}}\ar[d]^{query} & *++[F]{state}\ar[d]^{query}\\
  *++[F]{qstate} \ar[r]^{\textrm{query-update}} & *++[F]{qstate}
}
\end{minipage}
\caption{\label{Fi:Update}The view update problem. Queries are
  expressed by formulas in a rich logic with transitive closure,
  but query-update is expressed essentially propositionally.}
%The query-update is specified using a formula in a weak quantifier-free without explicit transitive closure.}
\end{wrapfigure}
%\end{figure}
This paper shows that it is possible to reason about reachability between dynamically allocated memory locations in potentially cyclic, singly-linked and doubly-linked
lists using effectively-propositional reasoning. We present a novel method that can harness existing SAT solvers to verify reachability properties of programs that manipulate linked-list data structures, and to
produce a concrete counterexample whenever a  program does not satisfy its specification. 
This result is
surprising because the natural specification of such programs involves quantifiers, inductive
definitions and transitive closure, thus precluding first-order, automatic theorem provers from dealing with reachability in a complete way.

Two central observations underpin our method.
(i)~In programs that manipulate singly- and doubly-linked lists it is possible to express the `next' pointer in terms of the reachability relation between list elements. This permits direct use of recent results in descriptive complexity~\cite{Phd:Hesse03}: we can maintain reachability
with respect to heap mutation in a precise manner. Moreover, we can axiomatize reachability using quantifier free formulas.
(ii)~In order to handle statements which traverse the heap, we allow verification conditions (VCs) with $\forall^*\exists^*$ formulas so that they can be discharged by SAT solvers (as we explain shortly). However, we allow the programmer to only write assertions in a restricted fragment of FOL
that disallows formulas with quantifier alternations but allows reflexive transitive closure. 
The main reason is that invariants occur both in the antecedent and in the consequent of the VC for loops;
thus the assertion language has to be closed under negation. 
% whereas verification conditions are not.

The appeal to descriptive complexity stems from the fact that recently
it has been applied to the view-update problem in databases.  This
problem has a pleasant parallel to the heap reachability update
problem we are considering. In the view-update problem, the logical
complexity of updating a query wrt.~database modifications is lower
than computing the query for the updated database from scratch
(depicted in \figref{Update}). Indeed, the latter uses formulas with
transitive closure, while the former uses quantifier-free formulas
without transitive closure. In our setting, we compute reachability
relations instead of queries. We exploit the fact that the logical
complexity of adapting the (old) reachability relation to the updated
heap is lower than computing the new reachability relation from
scratch. The solution we employ is similar to the use of dynamic graph
algorithms for solving the view-update problem, where directed paths
between nodes are updated when edges are added/removed (e.g., see
\cite{EL:DI08}), except that our solution is geared towards
verification of heap-manipulating programs with linked data
structures.

\subsection*{Main Results}
%The results in our paper can be summarized as follows:\\
%\begin{itemize}
-- We define \AF, a new logic for expressing properties of programs, that is  an \textit{alternation free} sub-fragment of $\FOTC$ (i.e., first-order logic with transitive closure): alternation between universal and existential quantifiers in formulas is disallowed.
A distinguishing feature of \AF\ is that it allows relation symbols but does not allow direct application of function symbols.   
Atomic formulas of \AF\ may denote 
reachability relations between memory locations via pointers such as
$next$ and $prev$ fields in linked lists, or any other relations without transitive closure.
\\
-- We empirically show that loop invariants in many programs manipulating singly- and doubly-linked lists can be specified using \AF\ formulas.\\
-- We show that the effect of many procedures manipulating singly- and doubly-linked lists can be specified using
\AF\ formulas.
This result may require that the memory that the procedure manipulates be ``owned'' by its formal parameters.%%  or it may require that the number of
%% cutpoints~\cite{POPL:RinetzkyBRSW05} be limited.\ab{Unsatisfactory.}
\\
%% =======
%% This result may require that the memory that the procedure manipulates be ``owned'' by its formal parameters or it may require that some other
%% bound on sharing memory locations by different paths~\cite{POPL:RinetzkyBRSW05}.\\
%% >>>>>>> .r418
-- We show direct use of existing results in dynamic complexity~\cite{Phd:Hesse03} to prove that \AF\ formulas
are closed under weakest preconditions for statements which destructively update memory (e.g., $x.next := y$).%%  in potentially cyclic, singly- and
%% doubly-linked lists. \ab{Can we remove potentially cyclic...everything is about such linked structures. Can we say that in advance?}
\\
-- For statements that traverse the heap (e.g., $x:= y.next$), \AF\ formulas are \textit{not} closed under weakest preconditions.
For these cases we show that weakest preconditions are expressible in the \FOAE\ logic which generalizes \AF\ by permitting existential quantification inside universal quantification.
\FOAE\ formulas are decidable for validity since their negation has the form $\exists^* \forall^*$,
and fits in the Bernays-Sch\"{o}nfinkel fragment which is decidable for satisfiability~\cite{JAR:PiskacMB10}.
In fact, they can be checked with a SAT solver by replacing existential quantifiers with
constants, and universal quantifications by conjunctions over the constants.
Indeed, Z3~\cite{de2008z3} is complete for these formulas.  \\
-- We report on experiments with a tool that checks correctness of several, commonly used heap-manipulating structured programs, and that uses Z3 as back-end. The tool can determine whether or not program annotations (pre- and postconditions, loop invariants) are \AF\ formulas, and can 
check both safety and equivalence of procedures.
The tool is sound and also complete in the sense that it generates concrete
counterexamples for programs violating the VCs.
%\end{itemize}

This paper is accompanied by a technical report containing further
examples and proofs.
