\section{Weakest Preconditions of Atomic Heap Manipulating Statements}
\label{Se:WlpAtomic}

In this section we show how to express the weakest liberal preconditions of atomic heap manipulating statements using
\FOAE\ formulas, for programs that manipulate acyclic singly-linked lists. 
\tabref{Wprules} shows standard $\wlp$ computation rules (top part) and the 
corresponding rules for field update, field read and dynamic allocation (bottom part). The correctness of the rule for destructive field update is according to Hesse's thesis~\cite{Phd:Hesse03}.

% \subsection{Destructive Updates}
% \
% \tabref{Wprules} contains the rules for computing weakest preconditions of destructive pointer updates.

 \begin{figure}
\[\begin{array}{@{}c@{}}
 \begin{array}{|rcl|}
 \hline
 \wlp(\skipc, Q) &\eqdef& Q\\
 \wlp(x := y, Q) &\eqdef& \substitute{Q}{x}{y}\\
\wlp(S_1 ~; ~ S_2, Q) &\eqdef& \wlp(S_1, \wlp(S_2, Q))\\
\wlp(\IF ~ B ~ \THEN~ S_1  ~ \ELSE~ S_2, Q) &\eqdef& 
\semp{B}\land \wlp(S_1, Q)\;\lor \\ &&\lnot\semp{B}\land\wlp(S_2, Q) \\
\wlp(\WHILE ~ B ~ \{I\} ~ \DO~ S, Q) &\eqdef& I\\
%\hline
\end{array}\\\hline\hline
%
\begin{array}{|rcl|}
%\hline
% Destructive Updates
  \wlp(x.\nextf := \NULL, Q) &\eqdef&
      \substitute{Q}{\alpha \B{\nextf^*} \beta}{\alpha \B{\nextf^*}\beta \land (\lnot \alpha \B{\nextf^*}x \lor \beta \B{\nextf^*}x)} \\
  \wlp(x.\nextf := y, Q) &\eqdef& \lnot y\B{\nextf^*}x \land\\
      && \substitute{Q}{\alpha \B{\nextf^*} \beta}{\alpha \B{\nextf^*}\beta \lor (\alpha \B{\nextf^*}x \land y \B{\nextf^*}\beta)}\\
% Allocation
  \wlp(x := \NEW, Q) &\eqdef&
      \forall \alpha : \left(\Land_{p\in\Pvar\cup\{\nullv\}} \neg p \B{\nextf^*} \alpha\right) \implies
         \substitute{Q}{x}{\alpha} \\
% Traversal
 P_{\nextf^+} &\eqdef&
      s\B{\nextf^*}t \land s\neq t\\
  P_{\nextf} &\eqdef&
      P_{\nextf^+} \land 
       \forall \gamma : \substitute{P_{\nextf^+}}{t}{\gamma} \implies
                          t\B{\nextf^*}\gamma\\
  \wlp(x := y.\nextf, Q) &\eqdef&
      \forall \alpha : \substitutetwo{P_{\nextf}}{s}{y}{t}{\alpha} \to \substitute{Q}{x}{\alpha}\\
\hline
\end{array}
 \end{array}\]
\caption{\label{Ta:Wprules}Rules for computing weakest liberal preconditions for procedures annotated with loop invariants and postconditions.
$I$ denotes the loop invariant, $\semp{B}$ is the \AF\ formula for program conditions and $Q$ is the postcondition expressed as an \AF\ formula.
The top frame shows the standard $\wlp$ rules for While-language, the bottom frame contains
our additions for heap updates, memory allocation, and dereference.}
\end{figure}

\paragraph*{Field Dereference.}
The rationale behind the formula for $\wlp(x := y.\nextf, Q)$ is that if $y$ has a
successor, then the formula $Q$ should be satisfied when $x$ is replaced by this
successor.
The natural way to specify this is using the Hoare assignment rule
\[
\wlp'(x := y.\nextf, Q) \eqdef \substitute{Q}{x}{\nextf(y)}
\]
However, this rule uses the function \nextf\ and does not directly express reachability.
Instead we will construct a relation $r_\nextf$ such that
$r_\nextf(\alpha, \beta) \impliesBothWays \nextf(\alpha) = \beta$ and then use universal quantifications to ``access'' the value
\[
\wlp''(x := y.\nextf, Q) \eqdef \forall \alpha: r_\nextf(y, \alpha) \implies \substitute{Q}{x}{\alpha}
\]

Since $\nextf$ is acyclic, %we do not need a symbol for $r_\nextf$ --- 
we can express $r_\nextf$ in terms of $\nextf^*$ as follows.
First we observe that $\nextf(\alpha)\neq\alpha$.
%in this case $\alpha \B{\nextf^+} \beta \impliesBothWays \alpha \B{\nextf^*} \beta \land \alpha \neq \beta$.
Also, since $\nextf$ is a function, the set of nodes reachable from $\alpha$ is totally ordered by $\nextf^*$. 
Therefore, similarly to \secref{Inverting}, we can express $r_\nextf(\alpha, \beta)$ as the minimal node $\beta$ in this order where $\beta \neq \alpha$.
Expressing minimality ``costs'' one extra universal quantification.

In \tabref{Wprules}, formula $P_{\nextf}$ expresses $r_\nextf$ in terms of $\nextf^*$:
$P_{\nextf}$ holds if and only if there is a path of length 1 between $s$ and $t$ (source and target).
Thus, $\substitutetwo{P_{\nextf}}{s}{y}{t}{\alpha}$ is satisfied exactly when $\alpha=\nextf(y)$.
If $y$ does not have a successor, then $\substitutetwo{P_{\nextf}}{s}{y}{t}{\alpha}$ can only be $\true$
if $\alpha=\nullv$, hence $Q$ should be satisfied when $x$ is replaced by
$\nullv$, which is in line with the concrete semantics.
\iftr Lemma~\ref{Le:RecoverNext}\else A central lemma \fi in \trappref{Logical} shows that the formula $P_\nextf$ correctly defines $\nextf$ as a relation.

\paragraph*{Dynamic allocation.}
The rule $\wlp(x := \NEW, Q)$ 
expresses the semantic uncertainty caused by the behavior
of the memory allocator. We want to be compatible with any run-time memory management, so
we do not enforce a concrete allocation policy, but require that the allocated node meets
some reasonable specifications, namely, that it is different from all values stored in
program variables, and that it is unreachable from any other node allocated previously
(Note: for programs with explicit {\tt free()}, this assumption relies on
the absence of dangling pointers, which can be verified by introducing 
appropriate assertions; this is, however, beyond the scope of this paper).
