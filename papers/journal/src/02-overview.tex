\section{Overview}
\seclabel{Overview}

\lstset{language=Java,numbers=left,numberstyle=\small,basicstyle=\ttfamily,tabsize=2}

\begin{figure}
%\begin{wrapfigure}{l}{2.7in}
%\centering
\begin{minipage}[t]{2.7in}
\begin{lstlisting}
Node reverse(Node h) {
	Node i = h,
	     j = null;
	while  (i != null)  { 
		Node t = i.next;
		i.next = j;
		j = i;
		i = t;
	}
	return j;
}
\end{lstlisting}
\end{minipage}
%
\begin{tikzpicture}[>=latex,baseline=(base),
    ll/.style={draw,rectangle,minimum size=6mm,every ll},
    every ll/.style={on chain},
    node distance=6mm]
    
  \def\dy{1.5cm}

  \coordinate(base);

  \begin{scope}[start chain]
    \node[ll,label={above:$h$, $i$}](0) {0};
    \node[ll](1) {1};
    \node[ll](2) {2};
    \node[ll](3) {3};
    \foreach \u/\v in {0/1,1/2,2/3}
      \draw[->] (\u) -- (\v);
    \draw (3) -- +(.5,0) coordinate(tip); \ground{(tip)}
  \end{scope}

  \tikzset{yshift=-\dy}
  
  \begin{scope}[start chain]
    \node[ll,label={above:$h$, $j$}](0) {0};
    \node[ll,label={above:$i$}](1) {1};
    \node[ll](2) {2};
    \node[ll](3) {3};
    \foreach \u/\v in {1/2,2/3}
      \draw[->] (\u) -- (\v);
    \draw (0) -- +(-.5,0) coordinate(tip); \ground[rotate=180]{(tip)}
    \draw (3) -- +(.5,0) coordinate(tip); \ground{(tip)}
  \end{scope}

  \tikzset{yshift=-\dy}

  \begin{scope}[start chain]
    \node[ll,label={above:$h$}](0) {0};
    \node[ll,label={above:$j$}](1) {1};
    \node[ll,label={above:$i$}](2) {2};
    \node[ll](3) {3};
    \foreach \u/\v in {1/0,2/3}
      \draw[->] (\u) -- (\v);
    \draw (0) -- +(-.5,0) coordinate(tip); \ground[rotate=180]{(tip)}
    \draw (3) -- +(.5,0) coordinate(tip); \ground{(tip)}
  \end{scope}

  \tikzset{yshift=-\dy}

  \begin{scope}[start chain]
    \node[ll,label={above:$h$}](0) {0};
    \node[ll](1) {1};
    \node[ll,label={above:$j$}](2) {2};
    \node[ll,label={above:$i$}](3) {3};
    \foreach \u/\v in {1/0,2/1}
      \draw[->] (\u) -- (\v);
    \draw (0) -- +(-.5,0) coordinate(tip); \ground[rotate=180]{(tip)}
    \draw (3) -- +(.5,0) coordinate(tip); \ground{(tip)}
  \end{scope}

  \tikzset{yshift=-\dy}

  \begin{scope}[start chain]
    \node[ll,label={above:$h$}](0) {0};
    \node[ll](1) {1};
    \node[ll](2) {2};
    \node[ll,label={above:$j$}](3) {3};
    \foreach \u/\v in {1/0,2/1,3/2}
      \draw[->] (\u) -- (\v);
    \draw (0) -- +(-.5,0) coordinate(tip); \ground[rotate=180]{(tip)}
  \end{scope}

\end{tikzpicture}
\caption{\label{Fi:Reverse}%
A simple Java program that reverses a list, and an example trace.}
%\end{wrapfigure}
\end{figure}

$\vdots$

\figref{Reverse} presents a heap-manipulating Java program for {\it in-situ} reversal of a \ab{an acyclic} linked list.
Every node in the list has a $\nextf$ field that points to its successor.
If we imagine the entire heap as one big array,
we can model $\nextf$ as a function that maps a node in the list to its successor,
or to the special value $\bot$, indicating $\vnull$. Let $\Locs$ be the set of memory locations in the
heap, and let $\Locs_{\bot}=\Locs\cup\{\bot\}$. Then $\nextf: \Locs\to \Locs_{\bot}$.

By looking at a trace of the program's execution, it is easy to notice an
interesting invariant: one list segments starts at the node pointed to by $i$,
and one at the node pointed to by $j$, and they do not share any elements.
In the initial configuration, $j=\bot$, so $j$ points to an empty list segment.
In the final configuration, $i=\bot$, and $j$ points to a list containing all
the elements that were in the original list.

We can write a loop invariant such as the following:
%
\begin{equation}
  I_1 \quad \eqdef \quad
  \forall \alpha:\Locs. ~ \lnot\big(\nextrtc i \alpha \land \nextrtc j\alpha \big)
\end{equation}
%
where $\nextrtc x y$ means $\exists k:\mathbb{N}$ such that $\nextf^k(x)=y$.
The variable $\alpha$ is quantified over all memory locations $\Locs$, which do not include $\vnull$;
if we want to include $\vnull$ ($\bot$) we would explicitly specify $\alpha:\Locs_\bot$.

% At this point it might be most useful to try to convery the intution that reasoning with n* alone is
% easier than reasoning with n + tc.
% Explain that list segments are ``elastic'', so that you can forget about the length
% n is small-step, <n*> is big-step

To prove that $I_1$ is an invariant, we will show that it is invariant under each of 
the statements in the loop body. For example,
%
\begin{equation}
  \{I_1\} ~ \mbox{\lstinline{i.next = j}} ~ \{I_1\}
  \label{Eq:goal: I1 i.next=j I1}
\end{equation}

It means that if $I_1$ holds in some state, call that state $\pre{\sigma}$,
and if the statement \lstinline{i.next = j}
executes successfully resulting in a new state, call it $\sigma$,
then $I_1$ holds in $\sigma$. To show that it is true, let $\pre{n}$ be the
value of $\nextf$ in $\pre{\sigma}$, and $n$ the value of $\nextf$ in $\sigma$.
Notice that the values of $i$ and $j$ are not affected by this statement,
so we use $i$,$j$ to denote their values in both $\pre\sigma$ and $\sigma$.

The semantics of \lstinline{i.next = j}
can be summarized as:
%
\begin{equation}
  n = \pre{n}[i\mapsto j]
  \label{Eq:assign-relation-simple}
\end{equation}
%
where $[\mapsto]$ is a shorthand notation defined by
\[f[x\mapsto y] = \lambda v. ~ \begin{cases}y & v = x \\ f(v) & \otherwise\end{cases}\]

That is, $n$ coincides with $\pre n$ anywhere except (perhaps) $i$, 
where it takes the value $j$.

With this information we can assign axiomatic semantics to the pointer field
assignment \lstinline{i.next = j}
in a way reminiscent of the assignment rule in Hoare logic:
%
\begin{equation}
  \renewcommand\arraystretch{1.5}
  \begin{array}{@{~~}c@{~~}} \hline
    \big\{P\big[\nextf[i\mapsto j]\big/\nextf\big]\big\} ~
    \mbox{\tt i.next = j} ~
    \big\{P\big\}
  \end{array}
  \label{Eq:assign-rule-simple}
\end{equation}

\begin{figure}
\[
  \renewcommand\arraystretch{1.7}
  \begin{array}{@{~~}c@{~~}} 
	  \models I_1 \limplies I_1\big[\nextf[i\mapsto j]\big/\nextf\big]
    \quad
	  \begin{array}{@{~~}c@{~~}} \hline
	    \big\{I_1\big[\nextf[i\mapsto j]\big/\nextf\big]\big\} ~
	    \mbox{\tt i.next = j} ~
	    \big\{I_1\big\}
	  \end{array}
	\quad
	  \models I_1 \limplies I_1 \\
	\hline
      \{I_1\} ~ \mbox{\tt{i.next = j}} ~ \{I_1\}
  \end{array}
\]
\caption{\label{Fi:hoare proof: I1 i.next=j I1}
  An example Hoare proof with pointer assignment using the regular assignment rule.}
\end{figure}

The function symbol $\nextf$ is substituted by the \emph{function expression}
$\nextf[i\mapsto j]$ in $P$. \figref{hoare proof: I1 i.next=j I1} shows
an example proof of \equref{goal: I1 i.next=j I1}. It depends on our
ability to prove validity of $I_1 \limplies I_1\big[\nextf[i\mapsto j]\big/\nextf\big]$,
which expands to
%
\[
\Big(\forall \alpha:\Locs. ~ \lnot\big(\nextrtc i \alpha \land \nextrtc j\alpha \big)\Big) \limplies
\Big(\forall \alpha:\Locs. ~ \lnot\big(i \abra{\nextf[i\mapsto j]^*} \alpha \land j\abra{\nextf[i\mapsto j]^*}\alpha \big)\Big)
\]

The presence of the transitive closure operator ``$^*$'' makes reasoning hard,
and in particular, makes this proof hard to automate using existing tools,
which mainly support first-order logic with some extensions (such as arrays or arithmetic).

We propose a different approach, by noticing the following, slightly more
complicated relation between $\pre{n}$ and $n$:

\begin{equation}
  \forall \alpha,\beta. ~ \nrtc\alpha\beta \liff
    \begin{cases}
      \big(\unrtc\alpha\beta \land \lnot \untc i\beta\big) \lor \unrtc j\beta & \unrtc\alpha i \\
      ~\unrtc\alpha\beta & \otherwise
    \end{cases}
\end{equation}

\begin{figure}[b]
  \centering
  \includegraphics[width=5cm]{img/update-assign}
  \caption{\label{Fi:update: i.next=j}
    An illustration of how the set of locations reachable from $\alpha$
    changes as a result of \lstinline{i.next = j} in the case where
    $\unrtc\alpha i$.}
\end{figure}

The notation $\untc x y$ means $\exists k\!>\! 0$ such that $\pre{n}^k(x)=y$.
It is important to mention that this relation holds because we
are talking about $\vnull$-terminated linked lists, which do not
allow cycles. We defer the discussion of cycles to keep the exposition
simple. \figref{update: i.next=j} conveys the intuition behind this
formula in the first case shown ($\unrtc \alpha i$); it is easy to see 
that the set of memory locations that are reachable from $\alpha$
is those that were reachable before, minus the ones that were reachable
from $\pre{n}(i)$, and with the addition of those reachable from $j$.

The assignment rule \equref{assign-rule-simple} can now be rewritten as follows;
for readability, we use $n$ instead of $\nextf$.

\begin{equation}
  \renewcommand\arraystretch{1.5}
  \begin{array}{@{~~}c@{~~}} \hline
    \left\{P\left[    
    \begin{cases}
      \big(\nrtc\alpha\beta \land \lnot \ntc i\beta\big) \lor \nrtc j\beta & \nrtc\alpha i \\
      ~\nrtc\alpha\beta & \otherwise
    \end{cases}
    \bigg/\nrtc\alpha\beta\right]\right\} ~
    \mbox{\tt i.next = j} ~
    \big\{P\big\}
  \end{array}
  \label{Eq:assign-rule-reach}
\end{equation}

This rule has a side condition, which is that $n$ \emph{never occurs in a term}
of $P$ as a function symbol, only as part of a formula $\nrtc x y$ or $\ntc x y$. \ab{Instead of side condition just observe that there is $n$ as a function symbol never occurs. Transition into logic with only relational symbols and no function symbols?}
This property is preserved across the substitution, because the usages of $n$ 
introduced by the rule are also of this form.

