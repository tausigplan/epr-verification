
\subsection{Extensions of EPR with functions}


It is well-known that validity of $\forall\exists$-formulas over a relational vocabulary (only relation and constant symbols) with equality is decidable. We try to recognize some general cases in which the addition of unary function symbols preserves the decidability. Moreover, as we prefer avoid devising new decision procedures, we are interested in performing reductions to EPR, and apply an off-the-shelf EPR-solver.

Assume a first-order signature $\Sigma_{\cal F}$ consisting of relation symbols, constant symbols and a set ${\cal F}$ of unary function symbols.
Suppose we are given a $\forall\exists$-formula $\phi_{\cal F}$ and a (finite) set $\Gamma_{\cal F}$ of universal formulas
in this signature.
We are interested to decide whether $ \Gamma_{\cal F} \vdash \phi_{\cal F}$ (i.e. $\phi_{\cal F}$ follows from $\Gamma_{\cal F}$ in FOL).
Initially, unary function symbols can be replaced by binary relations (that correspond to the graphs of the functions). We first produce an equivalent unnested formula $\phi$ and theory $\Gamma$ (in which all atomic formulas have the form $R(x_1 \til x_k)$, $f(x) \approx y$, or $c\approx y$). By choosing the quantifiers correctly we can ensure that $\phi$ and $\Gamma$ remain a $\forall\exists$-formula and a set of universal formulas respectively (e.g. in $\phi$ we only add universal quantifiers in negative occurrences and existential quantifiers in positive occurrences). Next, for every function symbol in ${\cal F}$, we replace in $\Gamma$ and $\phi$ the atomic formulas of the form $f(x) \approx y$  by $R_f(x,y)$, where $R_f$ is a new binary relation symbol.
We denote the new signature (with binary relations instead of unary functions) by $\Sigma$.
 Each new relation $R_f$ should have two properties:
\begin{itemize}
\item $PF^f = \forall x,y,z. R_f(x,y) \land R_f(x,z) \to y\approx z$ 
\item $TOT^f= \forall x. \exists y. R_f(x,y)$ 
\end{itemize}
We now have that $ \Gamma_{\cal F} \vdash \phi_{\cal F}$ iff 
$PF , TOT , \Gamma \vdash \phi$
where
$PF=\bigwedge_f PF^f$ and $TOT=\bigwedge_f TOT^f$.
$PF$ and $\Gamma$ are universal and cause no problem.
On the other hand, $TOT$ is $\forall\exists$, and thus it cannot work as an assumption
(entailment of $\forall\exists$ formulas is decidable when the assumptions are  $\exists\forall$ formulas).

We will assume certain (universal) properties on our functions (that are naturally translated to assumptions on the new binary relations), that will allow us to get rid of $TOT$.

\begin{notation}
Given a formula $\psi$ and a formula $P(x)$ (with one free variable $x$),
$\psi_{P(x)}$ denotes the relativization (=restriction) of $\psi$ to $P(x)$.
\end{notation}

\begin{proposition}
\label{function_elimination_proposition}
Suppose that there is an existential formula $P(x)$ (over $\Sigma$) such that the following hold:
\begin{enumerate}
\item $ PF , TOT , \Gamma\vdash \forall x. P(x)$.
\item $PF , \Gamma \vdash TOT^f_{P(x)}$ (that is $ PF ,\Gamma \vdash \forall x. \left( P(x) \to \exists y. \left(P(y) \land R_f(x,y)\right)\right)$) for every function symbol $f\in{\cal F}$.
\item $PF, \Gamma \vdash P(c)$ for every constant symbol $c$.
\item $PF, \Gamma \vdash \exists x. P(x)$
(this follows from the previous item, provided that there is at least one constant symbol).
\end{enumerate}
Then $PF, TOT, \Gamma \vdash \phi$ iff
$PF, \Gamma \vdash \phi_{P(x)}$.
\end{proposition}

Note that if $\phi$ is $\forall\exists$ and $P(x)$ is existential, then $\phi_{P(x)}$ is $\forall\exists$. Since $PF$ and $\Gamma$ are universal, it is decidable whether $PF, \Gamma\vdash \phi_{P(x)}$ or not. This way we can decide on $PF, TOT, \Gamma \vdash \phi$.

\begin{proof}
We denote by $M_{P(x)}$ the substructure of a first-order structure $M$ obtained restricting the domain of $M$ to all elements that satisfy $P(x)$ (assuming a relational vocabulary and ${M \models \exists x. P(x)} $). Note that $M_{P(x)} \models \psi$ iff $M \models \psi_{P(x)}$ for every formula $\psi$ and structure $M$ such that $M \models P(c)$ for every constant symbol $c$ and $M \models \exists x. P(x) $.

Suppose that $PF, TOT, \Gamma \vdash \phi$.
Let $M$ be a model of $PF$ and $\Gamma$.
Property $2$ ensures that $M \models TOT_{P(x)}$.
By properties $3$ and $4$, we also have $M \models P(c)$
for every constant symbol $c$, and $M \models \exists x. P(x)$.
Hence $M_{P(x)} \models TOT$.
Since $PF$ and $\Gamma$ are universal, we also have ${M_{P(x)} \models PF}$ and ${M_{P(x)} \models \Gamma}$.
It follows that $M_{P(x)} \models \phi$, and so $M \models \phi_{P(x)}$.

For the converse, suppose that $PF, \Gamma \vdash \phi_{P(x)}$.
Let $M$ be a model of $PF$, $TOT$ and $\Gamma$.
Hence, $M \models \phi_{P(x)}$.
In addition, properties $3-4$ ensure that 
$M \models P(c)$ for every constant symbol $c$, 
and  $M \models \exists x. P(x)$.
This implies that $M_{P(x)} \models \phi$.
Property $1$ ensures that $M \models \forall x. P(x)$, and hence $M_{P(x)}=M$.
It follows that $M \models \phi$.
\end{proof}

\begin{example}
\label{idempotent}
Suppose that we have exactly one function symbol $f$ and one constant symbol $null$,
and $\Gamma$ implies $R_f(null,null) \land \forall x, y. R_f(x,y) \to R_f (y,y)$
(i.e., $f(null)=null$ and $f$ is idempotent).
We can choose ${P(x)=\exists z. R_f (x,z)}$.
Then, $\forall x. P(x)= TOT$, and thus property $1$ obviously holds.
Property $2$ also holds:
$\forall x, y. R_f(x,y) \to R_f (y,y) \vdash  \forall x. \left( (\exists z. R_f (x,z)) \to \exists y. \left((\exists z. R_f (y,z)) \land R_f(x,y)\right)\right)$.
Properties $3-4$ clearly hold as well.
\end{example}

The following corollary of Proposition \ref{function_elimination_proposition} is particularly useful.

\begin{corollary}
\label{function_elimination_corollary}
Let $f_1 \til f_n$ be $n$ function symbols from ${\cal F}$.
For every  variable $x$,
let $P(x)$ denote the conjunction  $\bigwedge_{1\leq k\leq n} \exists y. R_{f_k}(x,y)$.
Suppose that each of the following formulas follows from $PF$ and $\Gamma$:
\begin{enumerate}
\item 
For every function symbol $f\in {\cal F}$: \\
$\forall x,  x_1 \til x_n.\; \bigwedge_{1\leq k\leq n} R_{f_k}(x,x_k) \to \bigvee_{1 \leq i \leq n} R_f(x,x_i)$.
\item 
For every constant symbol $c$:
$P(c)$.
\item $\exists x. P(x)$ (this follows from the previous item, provided that there is at least one constant symbol).
\end{enumerate}
Then $PF, TOT, \Gamma \vdash \phi$ iff $PF, \Gamma \vdash \phi_{P(x)}$.
\end{corollary}

\subsection{Implementation on Top of SMT}

In order to continue using Z3 without having to instrument it, we have
implemented the entire transformation as a preprocessing phase.
The user declares a set of unary functions $f_i : V \to V$ over the
domain $V$ of heap locations. Every formula then undergoes these
transformations:

\begin{enumerate}
  \item Un-nesting. Terms are reduced to contain only one function
   application; that is, function terms take the form $f(x)$ where
   $x$ is a variable or a constant. Atomic formulas are reduced so
   that the only ones containing functions take the form $f(x)=y$
   where $x$ and $y$ are variables or constants. So, for example,
   $R(f(g(x)), h(y))$ becomes 
   \[\forall\alpha,\beta,\gamma: g(x)=\alpha \land f(\alpha)=\beta \land
     h(y)=\gamma \limplies R(\beta,\gamma)\]
  \item Relationalization. Every quantified variable is bounded
   by the formula $\varphi$ saying that it has images in all declared
   functions:
   \[\varphi(\alpha) = \Land_{f\in F}\exists \beta:f(\alpha)=\beta\]
   For example, $\forall \alpha:R(x,\alpha)$ would be
   \[\forall\alpha:(\exists \beta:f(\alpha)=\beta) \land
     (\exists \beta:g(\alpha)=\beta) \land
     (\exists \beta:h(\alpha)=\beta) \limplies R(x,\alpha)\]
   The connective would be $\limplies$ for universal quantification 
   $\forall\alpha$ and $\land$ for existential $\exists\alpha$.
  \item Vocabulary outfitting. Every function $f\in F$ is associated
   with a new binary predicate symbol $R_f \subseteq V\times V$,
   and every atomic formula of the form $f(x)=y$ is rewritten as
   $R_f(x,y)$. 
\end{enumerate} 

\subsection{Cyclic Linked List}
\begin{itemize}
\item The commands speak about $n$ field, and $\tup{n^*}$ is used in the assertions.

\item Behind the scenes we have an acyclic relation $\langle k^* \rangle$,
and an auxiliary binary relation denoted by $\tup{m}$.

\item $\tup{k^*}$ is axiomatized with $\Gamma_{lin}$ as we had before.
\item Extra Axioms for $\tup{m}$:
\begin{itemize}
\item $(A)$ $\forall \alpha,\beta,\gamma.~ \alpha \tup{k^*} \beta \land \alpha  \tup{m} \gamma \to \alpha=\beta$
(there cannot be $k$ and $m$ from the same node)
\item $(B)$  $\forall \alpha,\beta,\gamma.~ \alpha \tup{m} \beta \land \alpha \tup{m} \gamma \to \beta=\gamma$ ($m$ is a partial functions)
\item $(C)$  $\forall \alpha,\beta.~ \alpha \tup{m} \beta \to \beta \tup{k^*} \alpha$ ($m$ is only used in cycle-closing edges.
\item $(D)$ $\neg (null \tup{m} null)$.
\end{itemize}

\item Additionally, two unary function symbols are used: $\vec{k}$ and $\vec{km}$.
\begin{itemize}
\item $\vec{k}( \alpha)$: 
Intended meaning: the last node reachable from $ \alpha$ via $k$-edges.
\item $\vec{km}( \alpha)$: 
Intended meaning: the node reachable from $\vec{k}( \alpha)$ via $m$-edges.
\end{itemize}

\item Axiom:
$$\forall u.\; ((  u \tup{k^*}\vec{k}(u) \land  \vec{k}(u) \tup{m}\vec{km}(u) \land \vec{km}(u) \tup{k^*} u) \lor (u = \vec{k}(u) \land u = \vec{km}(u) \land \forall v, w.\; ( u \tup{k^*} v \land v \tup{m} w \to \neg w \tup{k^*} u)))$$

\item $\alpha \tup{n^*} \beta$ is translated to: $\alpha \tup{k^*} \vec{k}(\beta)$.

\item $\alpha \tup{n} \beta$ is translated to: $\alpha \tup{k} \beta \lor \alpha \tup{m} \beta \lor (\beta = null \land \forall \gamma.\; \neg (\alpha \tup{k^+} \gamma) \land \neg (\alpha \tup{m}\gamma)) $.

\item A predicate $OnCycle(\alpha)$ is defined by:
$\vec{k}(\alpha) \tup{m} \vec{km}(\alpha)$

\item A predicate $SelfLoop(\alpha)$ is defined by:
$\alpha \tup{m} \alpha$

\item A predicate $Between(\alpha,\beta,\gamma)$ standing for ``there is a simple path
from $\alpha$ to $\gamma$ through $\beta$" can be defined by:
$$Between(\alpha,\beta,\gamma):= (\alpha \tup{k^*} \beta \tup{k^*} \gamma)
\lor (\alpha \tup{k^*} \vec{k}(\beta) \land \beta \tup{k^*} \gamma \land \neg \alpha \tup{k} \gamma) \lor (\alpha \tup{k^*} \beta \tup{k^*} \vec{k}(\gamma) \land \neg \alpha \tup{k} \gamma)$$

Using $Between(\alpha,\beta,\gamma)$ only, the programmer is not aware of our internal troubles with $k,m$ and $\vec{km},\vec{k}$.

\item Weakest precondition:

\begin{itemize}
\item $x.n := y$ (assuming that $x.n = null$):
\begin{itemize}
\item substitute $\alpha \tup{k^*} \beta$ by
$\alpha \tup{k^*} \beta \lor (y \neq null \land (\neg y \tup{k^*} x) \land (\alpha \tup{k^*} x ) \land (y \tup{k^*} \beta))$
\item substitute $\alpha \tup{m} \beta$ by $\alpha \tup{m} \beta \lor (y \tup{k^*} x \land \alpha =x \land \beta = y)$
\item substitute $\vec{k}(\alpha)$ by $ite(y \tup{k^*} \alpha \tup{k^*} x, x,\vec{k}(\alpha)$
\item substitute $\vec{km}(\alpha)$ by $ite(y \tup{k^*} \alpha \tup{k^*} x, y,\vec{km}(\alpha))$
\end{itemize}
\item $y := x.n$:
\begin{itemize}
\item $y \neq null$
\item add fresh symbol $z$, with axiom $y \tup{n} z$, and substitute $x$ by $z$.

\item $x.n := null$:

\begin{itemize}
\item $x \neq null$
\item Let $\alpha \tup{nk^*} \beta$ be defined by:
$(\alpha = \beta)  \lor
(x \neq \alpha \land \alpha \tup{k^*} \beta \tup{k^*} x) \lor
((\neg \alpha \tup{k^*} x) \land \alpha \tup{k^*} \beta) \lor
((\neg \alpha \tup{k^*} x) \land  \alpha \tup{n^*} \beta \land \beta \tup{k^*} x)$.
\item substitute $\alpha \tup{k^*} \beta$ by $\alpha \tup{nk^*} \beta$
\item substitute $\alpha \tup{m} \beta$ by $\alpha \tup{m} \beta  \land \neg \beta \tup{k^*} x$
\item substitute $\vec{k}(\alpha)$ by $ite(\alpha \tup{nk^*} x, \alpha, \vec{k}(\alpha))$
\item substitute $\vec{km}(\alpha)$ by $ite(\alpha \tup{nk^*} x, \alpha, \vec{km}(\alpha))$
\end{itemize}
\end{itemize}
\end{itemize}
\end{itemize}

\subsection{Doubly Cyclic Linked List}

\subsection{Nested Linked List}

\begin{itemize}

\item The commands speak about $d$ (down) and $r$ (right) field.
\item The assertions may include $\tup{d^*}$, $\tup{r^*}$, $\tup{(d\cup r)^*}$.

\item This only works when there is no sharing ---
if $r(\alpha)=\beta$ then: $r(\gamma)=\beta$ iff $\gamma=\alpha$, and $d(\gamma)\neq\beta$ for every $\gamma$.
\begin{itemize}
\item Add to the pre-condition of $x.r := y$ the fact that $y$ does not become shared:
$\forall \alpha. ~ (\alpha \tup{d^*} y \to \alpha=y) \land (\alpha \tup{r^*} y \to \alpha=y)$. 
Make this a conjunct in the $wp$ of $x.r := y$.

\item Add to the pre-condition of $x.d := y$ the fact that $y$ does not become shared:
$\forall \alpha. ~ (\alpha \tup{r^*} y \to \alpha=y)$. 
Make this a conjunct in the $wp$ of $x.d := y$.

\end{itemize}

\item One unary function symbol are used: $\overleftarrow{r}$.
\begin{itemize}
\item $\overleftarrow{r}(\alpha)$: 
\begin{itemize}
\item Intended meaning: the first node that can reach $ \alpha$ via $n$.
\item Axiom: $\forall \alpha,\beta. \beta \tup{r^*} \alpha \to \overleftarrow{r}(\alpha) \tup{r^*} \beta$
\item Idempotence: $\forall \alpha.~\overleftarrow{r}(\overleftarrow{r}(\alpha))=\overleftarrow{r}(\alpha)$ 
\end{itemize}


\end{itemize}

\item $\alpha \tup{(d\cup r)^*} \beta$ in the assertions is translated to: $\alpha \tup{n^*} \overleftarrow{r}(\beta)$.



\end{itemize}

