
\section{Generating an $\FOAE$ Verification Condition}
\label{Se:VCgen}
 \tabref{VCgen} provides the standard rules for computing VCs using weakest liberal preconditions.
An auxiliary function $\vcaux$ is used for defining the set of side conditions for the loops occurring in the program.
These rules are standard and their soundness and relative completeness have been discussed elsewhere (e.g. see \cite{frade2011verification}).

We assume that the effect, $\semp{B}$, of the condition $B$ used in the conditional and the while loop, is defined by an \AF\ formula.
We also assume that all loop invariants $I$, the precondition $P$, and postcondition $Q$ are \AF\ formulas.
The rule for $\WHILE$ loop is split into two parts: in the $\wlp$ we take just the loop invariant, where $\vcaux$ asserts that
loop invariants are inductive and implies the postcondition for each loop.

The rules may generate exponential formulas. Another solution can be implemented either using the method of Flanagan and Saxe~\cite{POPL:FlanaganS01} or by using a set of symbols for every program point.

%
 \begin{figure}
 \[
 \begin{array}{|c|}
 \hline
 \vcaux(S, Q) \eqdef \varnothing \hspace{0.7cm}\mbox{\small (for any atomic command $S$)} \\
 \vcaux(S_1;S_2, Q) \eqdef \vcaux(S_1, \wlp(S_2, Q)) \cup \vcaux(S_2, Q)\\
 \vcaux(\IF ~ B ~ \THEN~ S_1  ~ \ELSE~ S_2, Q) \eqdef \vcaux(S_1, Q) \cup \vcaux(S_2, Q)\\
 \vcaux(\WHILE ~ B ~ \{I\} ~ \DO~ S, Q) \eqdef \vcaux(S, I) \;\cup \hspace{1.7cm}{}\\ \hspace{3.4cm}\{I \land \semp{B} \implies \wlp(S, I), I \land \neg \semp{B} \implies Q\}\\
\hline
\vcgen(\{P\} S \{Q\}) \eqdef \big(P \implies \wlp(S, Q)\big) \land \Land \vcaux(S, Q)\\
\hline
\end{array}
\]
\caption{\label{Ta:VCgen}Standard rules for computing VCs using weakest liberal preconditions for procedures annotated with loop invariants and pre/postconditions.
%$I$ denotes the loop invariant, $\semp{B}$ is the \AF\ formula for program conditions and $Q$ is the postcondition.
%The rules for computing $\wlp$ appear in \tabref{Wprules}.
%% The auxiliary function $\vcaux$ accumulates a conjunction of VCs for the correctness of loops.
}
\end{figure}
Notice that \tabref{VCgen} only uses weakest liberal preconditions in a positive context without negations.
Therefore, the following proposition (proof in \trappref{Logical})
holds.
\begin{proposition}[VCs in \FOAE]
\label{prop:vcs}
For every program $S$ whose precondition $P$, postcondition $Q$,
branch conditions, loop conditions, and loop invariants are 
all expressed as \AF\ formulas, $\vcgen(\{P\} S \{Q\})$ is in \FOAE.
% For every program $S$ with precondition $P$, postcondition $Q$,
%  branch and loop conditions whose
%  semantics are $\semp{B_i}$, and loop invariants $I_i$, all expressed as \AF\ formulas,
%  $\vcgen(\{P\} S \{Q\})$ is in \FOAE.
\end{proposition}

% A note on optimization

\paragraph*{Optimization remark.}
The size of the VC can be significantly reduced
if instead of syntactic substitution, we introduce a new vocabulary for
each substituted atomic formula, axiomatizing its meaning as a separate formula.
For example, $\substitute{Q}{\alpha\B{\nextf^*}\beta}{P(\alpha,\beta)}$
(where $P$ is some formula with free variables $\alpha$, $\beta$), can be
written more compactly as
$\substitute{Q}{\alpha\B{\nextf^*}\beta}{r_1(\alpha,\beta)} \land
  \forall \alpha,\beta: r_1(\alpha,\beta)\impliesBothWays P(\alpha,\beta)$, where $r_1$ is a fresh relational symbol.
When $Q$ contains many applications of $\B{\nextf^*}$ and $P$ is large, this
may save a lot of formula space; roughly, it reduces the order of the VC size
from quadratic to linear.
 Our original implementation employed this optimization, which is also nice for finding bugs ---
 when the program violates the invariants the SAT solver produces a counterexample with the concrete states at every
 program point. The approach of~\cite{POPL:FlanaganS01} is also
 applicable in this case.
