\section{Extensions}
\label{sec:extensions}

\subsection{Explicit Memory Management}
\label{sec:explicit}

\newcommand\ifree{\mathit{free}}
\newcommand\tpush{\texttt{push}}
\newcommand\tpushMany{\texttt{pushMany}}
\newcommand\tdeleteAll{\texttt{deleteAll}}

\begin{changebar}
This section sketches how to handle explicit allocation and reclamation
of memory and exemplifies it on simple procedures shown in \figref{pushMany:code}
and \figref{deleteAll:code}. The procedures \tpush\ and \tpushMany\ extend a list
at the beginning by an unbounded of fresh elements allocated using {\tt new}.
The procedure \tdeleteAll\ takes as argument a list that has no foreign pointers
into it, and explicitly frees all elements. We verify that the procedures do not
introduce dangling pointers.

\begin{figure}
\begin{tabular}{@{}l@{ }l@{}}
@ requires & $\lnot \ifree(h)$\\
@ $\modset$ & $[h, h]_f$\\
@ ensures &  $\begin{array}{l}
	\forall \alpha : \ifree(\alpha) \liff \pre{\ifree}(\alpha)\land \alpha\neq\retval \\
	\frtc{\retval}{h} \land \lnot\frtc{h}{\retval}
\end{array}$
\end{tabular}
{\alltt
\begin{tabbing}
No\=de push(Node h) \{\+\\
Node e = new Node();\\
e.f = h;\\
return e;\-\\
\}\\
\end{tabbing}}
\begin{tabular}{@{}l@{ }l@{}}
@ requires & $\lnot \ifree(h)$\\
@ $\modset$ & $[h, h]_f$\\
@ ensures &  $\begin{array}{l@{\ }l}
	\forall \alpha : &\ifree(\alpha) \limplies \pre{\ifree}(\alpha) \\
	\forall \alpha : &\pre{\ifree}(\alpha) \land \lnot\ifree(\alpha) \limplies \\
		&\frtc{h}{\alpha} \land \ftc{\alpha}{\pre{h}}
  \end{array}$
\end{tabular}
{\alltt
\begin{tabbing}
No\=de pushMany(Node h) \{\+\\
if\= (?) \{\+\\
h = push(h);\\
h = pushMany(h);\-\\
\}\\
return h;\-\\
\}\\
\end{tabbing}}
\caption{\label{fi:pushMany:code}The procedure \tpush\ allocates a new element and inserts it to the beginning of the list.
The procedure \tpushMany\ calls \tpush on the same list an arbitrary number of times.}
\end{figure}

\begin{figure}
\begin{tabular}{@{}l@{ }l@{}}
@ requires & $\begin{array}{l}\forall \alpha,\beta : 
	\frtc{h}{\alpha} \land \frtc{\beta}{\alpha} \limplies
	\frtc{h}{\beta} \textit{\ \small/*\,dominance\,*/}\\
	\forall \alpha : \lnot \ftc{\alpha}{h}
\end{array}$\\
@ $\modset$ & $[h, \vnull)_f$\\
@ ensures &  $\begin{array}{l}
	\forallmod{\alpha,\beta} : \lnot\ftc{\alpha}{\beta} \\
	\forall \alpha: \ifree(\alpha) \liff \pre{\ifree}(\alpha) \lor \ufrtc{h}{\alpha}
\end{array}$
\end{tabular}
{\alltt
\begin{tabbing}
No\=de deleteAll(Node h) \{\+\\
if\= (h != null) \{\+\\
j = h.f;\\
h.f = null;\\
free(h);\\
deleteAll(j);\-\\
\}\-\\
\}\\
\end{tabbing}}
\caption{\label{fi:deleteAll:code}The procedure \tdeleteAll\ explicitly
reclaims the elements of a list dominated by its head, where no other 
pointers to this list exist.}
\end{figure}



\newcommand{\free}{\textit{free}}
\begin{table*}
\[
\begin{array}{||l|l|l|l|}
\hline\hline
\textbf{Command} & \textbf{Pre} &   \textbf{Mod} &\textbf{Post}\\
\hline
\texttt{$\retval$ = new()} &  \free(s) & \emptyset  & \retval = s \land \neg \free(s)\\
\hline
\texttt{access y} &   \neg \free(y) & \emptyset & \\
\hline
\texttt{free(y)} &  y \neq \vnull \land \neg \free(y) & \emptyset & \free(y)\\
\hline\hline
\end{array}
\]
\caption{\label{ta:AtomicSpecResource}%%
The specifications of atomic commands for resource allocations in a C like language.}
\end{table*}

\tabref{AtomicSpecResource} updates the specification of atomic commands (provided in \tabref{AtomicSpec}) to handle explicit memory management operations.
For simplicity, we follow Ansii C semantics but do not handle arrays and pointer arithmetic.
The allocation statement assigns a freed location denoted by $s$ to $\retval$ and sets its value to be non-freed.
All accesses to memory locations pointed-to by $y$ in statements \texttt{$\retval$ = y.f}, \texttt{y.f = x}, and \texttt{x = y} are required to access non-freed memory.
Finally, \texttt{free(y)} sets the free predicate to true for the node pointed-to by $y$. As a result, all the nodes reachable from $y$ cannot be accessed.

The adaptation rule needs to be augmented in order to accommodate
the change. Since nodes that are about to be allocated do not have
names, the $\modset$ shall refer only to allocated nodes; free nodes
can always be changed and they need not be specified in the $\modset$.
Of course, the procedure's post-condition should describe the new structure
of the allocated area in terms of reachability, if modular reasoning is
desired.

The change would be as follows: whenever there is a reference to some
$\sigma\not\in\modset$ (in \equref{out-in adaptation}, \equref{in-out adaptation}, and \equref{out-out adaptation}),
we would now consider only
$\sigma\not\in(\modset \cup \pre{\ifree})$.
This way the adaptation rule makes no claims regarding the free nodes.
Everything else remains just the same.
\end{changebar}

\subsection{Cyclic Linked-Lists}
\label{sec:Cycles}

For data structures with a single pointer, the acyclicity restriction may be
lifted by using an alternative formulation that keeps and maintains more
auxiliary information~\cite{Thesis:Bill,POPL:LH08}.
This can be easily done in \AFR, see~\cite{cav:IBINS13}.


\subsection{Doubly-linked lists}
\label{sec:Doubly}
To verify a program that manipulates a doubly-linked list, we need to support two fields $b$ and $f$.
\AFR\ supports this as long
as the only atomic formulas used in assertions are $\alpha\B{f^*}\beta$
and $\alpha\B{b^*}\beta$ (and not, for example,
$\alpha\B{(b|f)^*}\beta$).
In particular, we can specify the doubly-linked list property:
%
\begin{center}
$\forall \alpha, \beta: h\B{f^*}\alpha \land h\B{f^*}\beta \implies
  (\alpha\B{f^*}\beta \limpliesBidi \beta\B{b^*}\alpha)$.
\end{center}

Unfortunately modularity presents another challenge: how the modset should
be specified, and how to formulate the adaptation rule.
Since there are two pointer fields (forward and backward), the adaptation rule (\equref{adapt:fotc})
has to be instantiated twice. However that would require $\modset$ to be
defined as a union of intervals also according to $b$ in addition to its
being defined as such using $f$; otherwise our logical arguments from \secref{adapt:afar}
no longer hold.

When the input is a valid doubly-linked list this can always be done,
since $[\alpha,\beta]_f = [\beta,\alpha]_b$. In cases such where the
input is somewhat altered or corrupt (for example~\cite{POPL:RE11}),
the logic will have to be modified to incorporate the volatile exit
points of back-pointers potentially pointing to arbitrary nodes.
This extension is out of the current scope.
