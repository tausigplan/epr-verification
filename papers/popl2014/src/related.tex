\section{Related Work}
\label{sec:related}

\subsection{Modular Verification}
The area of modular procedure specification is heavily studied.
Many of these works require that the user declare potential changes similar to the modset (e.g., see \cite{Larch,JML,SpecS,PLDI:ZeeKR08}).
The frame rule of separation logic~\cite{POPL:IO01} naturally supports modular reasoning where the separating conjunction combines the local postcondition with
the assertion at the call site.
Unlike separation, reachability is a higher abstraction which relies on type correctness and naturally abstracts operations such as garbage collection.
Nevertheless, in \secref{explicit} we show that it can also deal with explicit memory reclamations.

We believe that our work in this paper pioneers the usage of an effectively propositional logic which is a weak logic to perform modular reasoning in a sound and complete way.
Our adaptation rule is more complex than the frame rule as it automatically updates reachability.
The idea of using the idempotent entry point function to enable local reasoning about list-manipulating programs (including an EPR reduction) has been explored independently by Piskac et al.~\cite{PiskacWZ13}, where it was used to automate the frame rule in separation logic.
In this paper we identify a general EPR fragment of assertions for which this idea of the idempotent entry point function is sound and complete.


\subsubsection{Cutpoints}
Rinetzky et al.~\cite{POPL:RinetzkyBRSW05} introduce cutpoint objects which are objects that can reach the area of the heap accessible by the procedure without passing through objects directly pointed-to by
parameters.
Cutpoints complicate program reasoning.
They are used in model checking~\cite{LMCS:FBQ11} and static analysis~\cite{SAS:RinetzkySY05,SAS:GotsmanBC06}.
Examples such as the ones in \cite{POPL:RE11} which include (unbounded) cutpoints from the stack are handled by our method without any changes.
These extra cutpoints cannot change the reachabilty and thus have no effect.
Interestingly, we can also handle certain programs which manipulate unbounded cutpoints.
Instead, we do limit the amount of new sharing in paths which are necessary for the verification. 
For example, the find procedure shown in \figref{UnionFind:code} includes unbounded sharing which can be created by the client program.
A typical client such as a spanning tree construction algorithm will indeed create unbounded sharing.
In the future, we plan to verify such clients by abstracting away the pointers inside the union-find tree.

\subsection{Decision Procedures}
Many decision procedures for reasoning about linked lists have been proposed~\cite{PLDI:MS01,JLP:Yorsh07,POPL:MPQ11,ATVA:BouajjaniDES12}.
All these logics are based on monadic second-order logic on trees which has a non-elementary time (and space) asymptotic complexity.
We follow \cite{cav:IBINS13} in using a weak logic which permits sound and complete reasoning using off the shelf SAT solvers which are efficient in practice
and can be implemented in polynomial space.
Indeed our preliminary experimental results reported \secref{experiments} show that Z3 is fast enough and may be even useful for automatically generating
abstract interpreters as suggested by~\cite{VMCAI:RSY04}.

Interestingly, the adaptation rule drastically simplifies the Weakest-Precondition rules given in \cite{cav:IBINS13}.
Notice the specifications in \tabref{AtomicSpec} do not use quantifiers at all,
whereas in \cite{cav:IBINS13} the formulas contain quantifiers with alternations.
Indeed the appropriate quantifiers are added in a generic manner by the adaptation rule
and weakest-precondition.

\subsection{Incremental Reachability Update}
Formulas for updating transitive closure w.r.t., graph mutations have been developed by various authors (e.g., \cite{DS95,Thesis:Bill,Book:Neil,POPL:LH08}).
These works assume that a single edge is added and deleted.
This submission generalizes these results to procedures which perform unbounded mutations.
Indeed our adaptation rule generalizes \cite{Thesis:Bill,POPL:LH08,cav:IBINS13}) which provides reachability update formula w.r.t. the removal of a single edge.
