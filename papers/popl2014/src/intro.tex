\section{Introduction}
\label{sec:intro}

%% Executive Summary
This paper shows how to harness existing SAT solvers for proving that a %given 
potentially recursive procedure satisfies its specification and for
automatically producing counterexamples when it does not.
We concentrate on proving safety properties of imperative programs manipulating linked data structures which is challenging since we need to reason about unbounded
memory and destructive pointer updates.
The tricky part is to identify a logic which is expressive enough to enable the modular verification of interesting procedures and properties
and weak enough to enable sound and complete verification using SAT solvers.

%% Effectively Propositional
Recently it was shown \cite{cav:IBINS13} how to employ effectively propositional logic (or BSR logic\footnote{Due to Bernays, Sch\"{o}nfinkel and Ramsey.}) for verifying programs manipulating
linked lists.
% Effectively propositional logic is also called BSR due to Bernays-Sch\"{o}nfinkel and Ramsey.
It decides the validity of formulas of the form $\forall^*\exists^* q$ using SAT solvers where $q$ is a quantifier free relational formula
(or equivalently decides the satisfiability of $\exists^*\forall^* q$ formulas).
It has been successfully used in many other contexts~\cite{JAR:PiskacMB10}.

\begin{figure*}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \def\dw{5mm}
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode,label={[name=h]left:\small$h$}] {1}; &
        \node(b)[listnode] {2}; &
        \node(c)[listnode] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode] {5}; \\
        &
        \node(z1)[listnode,label={[name=z1lbl]below:\small$z_1$}] {}; &
        \node(z2)[listnode,label={[name=z2lbl]below:\small$z_2$}] {}; &
        \node(z3)[listnode,label={[name=z3lbl]below:\small$z_3$}] {}; \\
    };
    \path[->] (h) edge (a) (a) edge (b) (b) edge (c) (c) edge (d) (d) edge (e)
        (z1lbl) edge (z1) (z1) edge[extpath] (b)
        (z2lbl) edge (z2) (z2) edge[extpath] (c)
        (z3lbl) edge (z3) (z3) edge[extpath] (d);
    \begin{pgfonlayer}{background layer}
      \node[modset,fit=(a)(b)(c)(d)(e)] {};
    \end{pgfonlayer}

    \node[symbol,label=above:{\small\texttt{reverse} $x$}] at(3.5cm,0.75cm) {$\leadsto$};

    \begin{scope}[xshift=7cm]
    \matrix[column sep=\dw, row sep=\dw, label distance=3mm, font=\tiny] {
        \node(a)[listnode] {1}; &
        \node(b)[listnode] {2}; &
        \node(c)[listnode] {3}; &
        \node(d)[listnode] {4}; &
        \node(e)[listnode,label={[name=h]right:\small$h$}] {5}; \\
        &
        \node(z1)[listnode,label={[name=z1lbl]below:\small$z_1$}] {}; &
        \node(z2)[listnode,label={[name=z2lbl]below:\small$z_2$}] {}; &
        \node(z3)[listnode,label={[name=z3lbl]below:\small$z_3$}] {}; \\
    };
    \path[->] (h) edge (e) (e) edge (d) (d) edge (c) (c) edge (b) (b) edge (a)
        (z1lbl) edge (z1) (z1) edge[extpath] (b)
        (z2lbl) edge (z2) (z2) edge[extpath] (c)
        (z3lbl) edge (z3) (z3) edge[extpath] (d);
    \begin{pgfonlayer}{background layer}
      \node[modset,fit=(a)(b)(c)(d)(e)] {};
    \end{pgfonlayer}
    \end{scope}
\end{tikzpicture}
\caption{\label{fi:ReverseSnapshot}%
Reversing a list pointed to by a head $h$ with many shared nodes accessible from outside the local
heap (surrounded by a rounded rectangle).}
\end{figure*}

%% Why doesn't it suffice
In this paper we show that effectively propositional logic does not suffice to naturally express the effect on the global heap when the local heap of a procedure
is accessible via shared nodes from outside.
For example, \figref{ReverseSnapshot} shows a pre- and post-heap before a list pointed-to by $h$ is reversed.
The problem is how to express the change in reachability between nodes such as $z_i$ and list nodes $1, 2, \ldots, 5$: note that, e.g., nodes $3, 4, 5$ are unreachable from $z_1$ in the post-heap.


This paper shows that in many cases, including the above example, reachability can be checked precisely using SAT solvers.
Our solution is based on the following principles:
\begin{itemize}
\item We follow the standard techniques (e.g., see \cite{Larch,JML,SpecS,PLDI:ZeeKR08}) by requiring that the programmer defines the set of
potentially modified elements.
\item The programmer only specifies postconditions on local heaps and ignores the effect of paths from the global heap.
\item We provide a general and exact \newterm{adaptation rule} for adapting postconditions to the global heap.
This adaptation rule is expressible in a generalized version of BSR called \AEAR.
\AEAR\ allows an extra \newterm{entry} function symbol which maps each node $u$ in the global heap into the first node accessible
from $u$ in the local heap.
In \figref{ReverseSnapshot},
 $z_1, z_2$ and $z_3$  are mapped to $2, 3$ and $4$, respectively.
The key facts are that \AEAR\ suffices to precisely define the global reachability relationship
after each procedure call and yet
any \AEAR\ formula
can be simulated by a BSR formula. Thus the automatic methods of \cite{cav:IBINS13}
still apply to this significantly more general setting.
\item We restrict the verified procedures in order to guarantee that the generated verification condition of every procedure remains in \AEAR.
The main restrictions are: type correctness, deterministic paths in the heap, limited number of changed list segments in the local heap (each of which may be unbounded) and
limited amount of newly created heap sharing by each procedure call.
These restrictions are enforced by the generated verification condition in $\AEAR$.
This formula is automatically checked by the SAT solver.
\end{itemize}


\subsection{Main Results}

The results in this paper can be summarized as follows:
\begin{itemize}
\item We define a new logic, \AEAR, which extends BSR with a limited idempotent function and yet is  equi-satisfiable with BSR.
\item We provide a precise adaptation rule in \AEAR, which
  expresses the locality property of the change, and in conjunction
  with the postcondition on the local heap,
  precisely updates the reachability relation of the global heap.
\item We generate a modular verification formula in \AEAR\ for each procedure, asserting that the
  procedure satisfies its pre- and post-conditions and the above restrictions.
This verification condition is sound and complete, i.e., it is valid if and only if the procedure
adheres to the restrictions and satisfies its requirements.
We implemented this tool on top of Z3.
\item We show that many programs can be modularly verified using our methods.
They satisfy our restrictions and their BSR invariants can be naturally expressed.
\end{itemize}

\subsection{A Running Example}

To make the discussion more clear, we start with an example program.
We use the union-find data structure\footnote{We have simplified \tunion\ by
  not keeping track of the sizes of 
  sets in order to attach the smaller set to the larger.},  which maintains a forest using a parent
pointer at each node (see \figref{UnionFind:code}) 
\cite{JACM:Tar75}.

The method \tfind\ requires that the argument $x$ is  not null.
The formula $\tail{f}(x, r_x)$ asserts that 
the auxiliary variable $r_x$ is equal to the root of $x$.
The procedure changes the pointers of some nodes in the closed interval $[x, r_x]_f$ to
\begin{changebar}
point directly to $r_x$. Intervals are formally defined later (\defref{interval}).
Intuitively, the closed interval $[a,b]_f$ denotes the set of nodes pointed to
by $a$, $a.f$, $a.f.f$ and so on up until $b$ inclusive.
\end{changebar}

The return value of \tfind\ (denoted by $\retval$) is $r_x$.
The postcondition uses the symbol $\upsinglSym$ that denotes the value of $f$ before the method was invoked.
Since \tfind\ compresses paths from ancestors of $x$ to single edges to $r_x$, 
this root may be shared via new parent pointers.
\figref{Find:scenario} depicts a typical run of \tfind.

The method \tunion\ requires that both its arguments are not null.
It potentially modifies the ancestors of $x$ and $y$, i.e., $[x, r_x]_f \cup [y, r_y]_f$.
\figref{Union:scenario} depicts a typical run of \tunion.
Notice that we support an unbounded number of cutpoints~\cite{POPL:RinetzkyBRSW05} (see \secref{related}).



\begin{figure}
\begin{tabular}{@{}l@{ }l@{}}
@ requires & $x \neq \vnull \land \tail{f}(x, r_x)$\\
@ $\modset$ & $[x, r_x]_f$\\
@ ensures &  $\begin{array}{l}\retval = r_x \land \\\
    \forallmod{\alpha, \beta}: \prtc{\alpha}{\beta} \liff \alpha = \beta \lor \beta = r_x\end{array}$
\end{tabular}
{\alltt
\begin{tabbing}
No\=de find(Node x) \{\+\\
Node i = x.f;\\
if\= (i != null) \{\+\\
i = find(i);\\
x.f = i;\-\\
\}\\
else \{\+\\
i = x;\-\\
\}\\
return i;\-\\
\}\\
\end{tabbing}}
\begin{tabular}{@{}l@{ }l@{}}
@ requires & $x \neq \vnull \land  y \neq \vnull \land \tail{f}(x, r_x) \land \tail{f}(y, r_y)$\\
@ $\modset$ & $[x, r_x]_f \cup  [y, r_y]_f$\\
@ ensures \\
\end{tabular}
$\forallmod{\alpha, \beta}:
    \begin{array}{l}
    (\uprtc{x}{\alpha}\limplies (\prtc\alpha\beta\limpliesBidi \\
            \multicolumn{1}{r}{\beta=\alpha \lor \beta=r_x\lor\beta=r_y))}\\
    \land (\uprtc{y}\alpha \limplies (\prtc\alpha\beta\limpliesBidi \beta=\alpha\lor\beta=r_y))
    \end{array}$
{\alltt
\begin{tabbing}
vo\=id union(Node x, Node y)\{\+\\
Node t = find(x); Node s = find(y);\\
if (t != s) t.f = s;\-\\
\}
\end{tabbing}}
\caption{\label{fi:UnionFind:code}%
An annotated implementation of Union-Find in Java.
$f$ is the backbone field denoting the parent of a tree node.}
\end{figure}

\newcommand\retvalmark{\tikz\node[listnode,retval] {};}

\begin{figure}
\centering
\begin{tikzpicture}
  \def\cw{3mm}
  \def\szA{4cm}
  \matrix {
  \node[listnode](x) at (0,0) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode](r) at (2,0) [label=right:$r$] {};
  \node[listnode](y) at (0,-0.7) [label=left:$y$] {};
  \draw[->] (x) -- (px); \draw[->] (px) -- (r);
  \draw[->] (y) -- (px); &
  \node[symbol,label=above:{\small\tfind\ $x$}] {$\leadsto$}; &
  \node[listnode](x) at (1,0.7) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode,retval](r) at (2,0) [label=right:$r$] {};
  \node[listnode](y) at (0,-0.7) [label=left:$y$] {};
  \draw[->] (x) -- (r); \draw[->] (px) -- (r);
  \draw[->] (y) -- (px);
  \\
  };
\end{tikzpicture}
\def\cw{2mm}
\caption{\label{fi:Find:scenario}An example scenario of running \tfind\
    (\,{\protect\retvalmark} {\small = return value}).}
\end{figure}


\begin{figure}
\centering
\begin{tikzpicture}
  \def\cw{3mm}
  \def\szA{4cm}
  \matrix[column sep=2mm] {
  \node[listnode](x) at (0,0) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode](r) at (2,0) [label=right:$t$] {};
  \node[listnode](w) at (0,-0.7) {};
  \node[listnode](y) at (0, 0.7) [label=left:$y$] {};
  \node[listnode](py) at (0.9,0.7) {};
  \node[listnode](s) at (1.8,0.7) [label=right:$s$] {};
  \path[->] (y) edge (py) (py) edge (s);
  \draw[->] (x) -- (px); \draw[->] (px) -- (r);
  \draw[->] (w) -- (px); &
  \node[symbol,label=above:{\small\tunion\ $x,y$}] {$\leadsto$}; &
  \node[listnode](x) at (1.3,-0.7) [label=left:$x$] {};
  \node[listnode](px) at (1,0) {};
  \node[listnode](r) at (2,0) [label=right:$t$] {};
  \node[listnode](w) at (0,-0.7) {};
  \node[listnode](y) at (0.3, 0.7) [label=left:$y$] {};
  \node[listnode](py) at (1.2,0.7) {};
  \node[listnode](s) at (2.2,0.7) [label=right:$s$] {};
  \draw[->] (x) -- (r); \draw[->] (px) -- (r);
  \draw[->] (w) -- (px);
  \path[->] (r) edge (s) (py) edge (s) (y) edge[out=30,in=150] (s);
  \\
  };
\end{tikzpicture}
\caption{\label{fi:Union:scenario}An example scenario of running \tunion.}
\end{figure}

\subsection{Working Assumptions}
\label{sec:working-assumptions}

\begin{description}
\item [Type correct] The procedure manipulates references to dynamically created objects in a
  type-safe way.  For example, we do not support pointer arithmetic.  
\item [Deterministic Reachability] The specification may use arbitrary uninterpreted relations.
It may also use the reachability formula $\frtc{\alpha}{\beta}$ meaning that
$\beta$ is reachable from $\alpha$ via zero or more steps along the functional backbone field $f$.
It may not use $f$ in any other way.
Until \secref{extensions}, we require $f$ to be acyclic and we restrict our attention to only one
backbone field.
\item [Precondition] There is a  \newterm{requires clause} defining the precondition which
   is written in alternation-free relational first-order logic ($\AFR$) and may use the relation
   $f^\star$ \cite{cav:IBINS13}.
\item [Mod-set] There is a \newterm{modifies clause} defining the mod-set ($\modf$), which is the set of
  potentially changed memory locations
(We include both source and target of every edge that is added or deleted). 
 The  modified set may have an unbounded number of vertices, but we require it to be the union of
a bounded number of $f$-intervals, that is chains of vertices through $f$-pointers.
\item [Postcondition] There is an \newterm{ensures clause} which exactly defines the new
  reachability relation $f^\star$ restricted to $\modf$.
The ensures clause, written in $\AFR$, may use two vocabularies (employing 
both $\ufsinglSym$ and $f$ to
refer to the reachability relations before and after.
\item [Bounded new sharing] All the new shared nodes --- nodes pointed to by more than one node ---
must be pointed to by local variables at the end of the procedure's execution.  This requires
that only a bounded number of new shared nodes can be introduced by each procedure call. Note that many heap-manipulating programs exhibit limited sharing as noted in the experimental measurements of Mitchell~\cite{MitchellSS09}. A similar
restriction is also used in shape analysis techniques for device driver programs~\cite{YangLBCCDO08}.
\item [Loop-free] We assume that all code is loop free, with loops replaced by recursive calls.
\end{description}

\subsection{Outline of the rest of this paper}

\secref{overview} provides a rule for adapting local changes to states containing a global heap.
The idea is that the programmer only specifies changes in a small, local area of the heap.
\secref{adaptable-logic} introduces a new logic called \AEAR.
\secref{modular-spec} formalizes the requirements for specifying the meaning of commands and procedures.
The technique for generating verification conditions is presented in \secref{VC}.
Extensions to the frameworks are discussed in \secref{extensions}.
Our preliminary verification experience appears in \secref{experiments}.
%In \secref{lower} we show why some of our restrictions seem necessary.
\secref{related} discusses related work and \secref{conclusion} concludes.
Details of the logical proof are contained in~\logicalannex.
