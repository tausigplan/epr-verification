

\section{Modular Specifications of Procedure Behaviours}
\label{sec:modular-spec}

%Here we explain how to write the procedure specifications.

\subsection{Notations}

\begin{definition} {\rm Let
$\voc=\B{\constants, \set{f}, \relations}$ be a vocabulary including
the  constant symbol $\vnull$.
A \textbf{state}, $M$, is a logical structure with domain $\dom{M}$, including $\vnull$, and
$\vnull^M = \vnull = f^M(\vnull)$,
\begin{changebar}
where $s^M$ is the interpretation of the symbol $s$ in the structure $M$.
\end{changebar}
A state is \textbf{appropriate} for an annotated procedure $\proc$ if its vocabulary includes every
symbol occurring in its annotations, and constants corresponding to 
all of the program variables occurring in $\proc$.}
\end{definition}

The diagrams in this paper denote states. For example \figref{ReverseSnapshot} shows a transition
between two states. 

\begin{changebar}
Below we define the notion of two-vocabulary structures, which are useful to
describe relations between pre- and post-states.

\begin{definition}[Two-vocabulary Structure]
For states $\pre{M}$ and $M$ over the same vocabulary $\voc=\B{\constants, \set{f}, \relations}$ 
and with the same domain ($\dom{\pre{M}}=\dom{M}$),
we denote by $\pre{M}/M$ the structure over the two-vocabulary
$\voc'=\B{\constants \cup \pre{\constants} \setminus \{\pre{\vnull}\}, \{f,\pre{f}\}, \relations \cup \pre{\relations}}$ 
obtained by combining $\pre{M}, M$ in the following way: 
$f$ is interpreted as $f^M$ and $\pre{f}$ is interpreted as $f^{\pre{M}}$,
and similarly for all the other symbols.
\end{definition}

\begin{definition}[Backbone Differences]
\label{de:backnoneDiff}
For states $\pre{M}$ and $M$ over the same vocabulary $\voc=\B{\constants, \set{f}, \relations}$ 
and with the same domain ($\dom{\pre{M}}=\dom{M}$),
the set $\pre{M}\oplus M$ consists of the ``differences between $\pre{M}$ and $M$ w.r.t. $f$, excluding $\vnull$'',
i.e. all elements $u$ of $\dom{M}$
such that $f^M(u)\neq f^{\pre{M}}(u)$, as well as their non-$\vnull$ images in $f^M$
and $f^{\pre{M}}$.
\end{definition}

For example, in \figref{Find:scenario}, let $\pre{M}$ be the left structure and $M$ the
right structure.  Then $M\oplus\pre{M}=\{x,r,\square\}$, where
$\square$ is the unlabeled node with an edge from $x$ in the left diagram.
\end{changebar}

\subsection{Modification Set}

We must specify the \textbf{mod-set}, $\modset$, containing all the endpoints of edges that are modified.
Therefore when adding or deleting an edge $\abra{s,t}$, both ends --- the source, $s$, and the target, $t$ ---
are included in $\modset$.
Often in programming language semantics, 
only the source is considered modified. However, thinking of 
the heap as a graph, it is useful to consider both ends of a modified edge as modified. 
\begin{changebar}
For example, in the running example program \tfind\ (\figref{UnionFind:code}),
since new references to $r_x$ may be introduced as a
result of path compression,
the root $r_x$ is also considered as part of $\modset$.
\end{changebar}

Our mod-sets are built from two kinds of intervals:

\begin{definition}[Intervals]
\label{de:interval}{\rm
The \newterm{closed interval} $[a,b]_f$ is
\[[a,b]_f \eqdefS \{\gamma \stS \frtcBetween{a}{\gamma}{b}\}\]
and  the \newterm{half-open interval} $[a,\vnull)_f$ is:
\[
[a,\vnull)_f \eqdefS \{
     \gamma \stS \frtc{a}{\gamma} \land \ftc{\gamma}{\vnull}\}
\]}
\end{definition}

(notice that $\frtc{\gamma}{\vnull}$ is always true in acyclic heaps).

\begin{definition}[mod-set]
\label{de:mod}
  The mod-set, $\modset$, of a procedure is a union $I_1\cup I_2\cup \ldots \cup I_k$,
  where each $I_i$ may be  $[s_i,t_i]_f$ or 
  $[s_i,\vnull)_f$, $s_i, t_i$ are
  parameters of the procedure or constant symbols occurring in the pre-condition.
\end{definition}

In our examples, the mod-sets of \tfind\ and \tunion\ are written above
each procedure, preceded by the symbol ``@ mod' (\figref{UnionFind:code}).
Note that it follows from Definition \ref{de:mod} that $\alpha\in\modset$,
is expressible as a quantifier-free formula.


\begin{definition}
\label{de:Mosets}
Given an appropriate state $M$ for $\proc$ with modset $\modset$,
$\modset^M$ is the set of all elements in $\dom{M}$ that are in
one of the intervals defining $\modset$ (see
\defref{interval}).
\end{definition}




\subsection{Pre- and Post-Conditions}

The programmer specifies \AFR\ pre- and post-conditions.
Two-vocabulary formulas may be used in the post-conditions where $\ufsinglSym$ denotes the value
of $f$ before the call. 

\subsection{Specifying Atomic Commands}

\begin{table*}
\[
\begin{array}{||l|l|l|l|}
\hline\hline
\textbf{Command} & \textbf{Pre} &   \textbf{Mod} &\textbf{Post}\\
\hline
\retval\texttt{ = y.f} &  y \neq \vnull \land E_f(y, s) & \emptyset  & \retval = s\\
\hline
\texttt{y.f = null} &  y \neq \vnull \land E_f(y, s) & [y, s]_f  & \neg \frtc{y}{s} \land \neg \frtc{s}{y}\\
\hline
\texttt{assume y.f==null}; & & &\\
\texttt{y.f = x} &  y \neq \vnull \land E_f(y, \vnull) \land \neg \frtc{x}{y} & [y, y]_f \cup [x, x]_f & \frtc{y}{x} \land \neg \frtc{x}{y}\\
\hline\hline
\end{array}
\]
\caption{\label{ta:AtomicSpec}%%
The specifications of atomic commands. $s$ is a local constant denoting the $f$-field of $y$.
$E_f$ is the inversion formula defined in \equref{Inversion}.}
\end{table*}

\tabref{AtomicSpec} provides specification of atomic commands.
They describe the memory changed by atomic statements and the changes on the local heap.

\paragraph{Accessing a pointer field}
The statement \texttt{ret = y.f} reads the content of the $f$-field of \texttt{y}, into \texttt{ret}.
It requires that \texttt{y} is not \texttt{null} and that an auxiliary variable \texttt{s} points to the $f$-field of \texttt{y} (which may be \texttt{null}).
It does not modify the heap at all.
It sets \texttt{ret} to \texttt{s}.
It is interesting to note that the postcondition is quantfier free and much simpler than the one
provided in \cite{cav:IBINS13}.  The reason is that we do not need to specify the effect on the
whole heap.  


\paragraph{Edge Removals}
The statement \texttt{y.f = null} sets the content of the $f$-field of \texttt{y}, to \texttt{null}.
It requires that \texttt{y} is not \texttt{null} and that an auxiliary variable \texttt{s} points to the $f$-field of \texttt{y} (which may be \texttt{null}).
It modifies the node pointed-to by \texttt{y} and potentially the node pointed-to by \texttt{s}.
Notice that the modset includes the elements pointed to by $y$ and $s$, the two end-points of the edge.
It removes paths between \texttt{y} and \texttt{s}.
The postcondition asserts that there are no paths from $y$ to $s$.
Also, since $s$ is potentially modified, it asserts that there are no paths from $s$ to $y$.

\paragraph{Edge Additions}
The statement \texttt{y.f = x} is specified assuming without loss of generality,  that
the statement \texttt{y.f = null} was applied before it.
Thus, it only handles edge additions.
It therefore requires that \texttt{y} is not \texttt{null} and its $f$-field is \texttt{null}.
It modifies the node pointed-to by \texttt{y} and potentially the node pointed-to by \texttt{x}.
It creates a new path from \texttt{y} to \texttt{x} and asserts the absence of new paths from \texttt{x} back to \texttt{y}.
Again the absence of back paths (denoted by $\neg \frtc{x}{y}$) is needed for completeness.
The reason is that both the node pointed-to by \texttt{x} and \texttt{y} are potentially modified.
Since \texttt{x} is potentially modified, without this assertion, a post-state in which \texttt{x.f == y} will be allowed by the postcondition.

\subsubsection{Soundness and Completeness}


We now formalize the notion of soundness and completeness of modular specifications and assert that the specifications of atomic commands are sound and complete.


\begin{definition}[Soundness and Completeness of Procedure Specification]
Consider a procedure $\proc$ with
 precondition $P$, modset $\modset$, post-condition $Q$.
We say that $\B{P, \modset, Q}$ \textbf{is sound with respect to} $\proc$ if for every appropriate pre-state $\pre{M}$ such that $\pre{M} \models P$,
and appropriate post-state $M$ which is a potential outcome of the body of $\proc$ when executed on $\pre{M}$:
(i)~$\pre{M} \oplus M\subseteq\modset^{\pre{M}}$,
(ii) $\pre{M}/M \models Q$.
Such a triple $\B{P, \modset, Q}$ \textbf{is complete with respect to} $\proc$ if for every appropriate states $\pre{M}, M$ such that
(i)~$\pre{M} \models P$,
(ii)~$\pre{M}/M \models Q$,
and
(iii)~$\pre{M} \oplus M\subseteq\modset^{\pre{M}}$,
then there exists an execution of the body of $\proc$ on $\pre{M}$ whose outcome is $M$.
\end{definition}

The following proposition establishes the correctness of atomic statements.
\begin{proposition}[Soundness and Completeness of Atomic Commands]
The specifications of atomic commands given in \tabref{AtomicSpec} are sound and complete.
\end{proposition}



The following lemma establishes the correctness of \tfind\ and \tunion, which is interesting since they update an unbounded amount of memory.
\begin{lemma}[Soundness and Completeness of Union-Find]
The specification of \tfind\ and \tunion\ in \figref{UnionFind:code} is sound and complete.
\end{lemma}

We can now state the following proposition:

\begin{proposition}[Soundness and Completeness of $\adapt{}$]
Let $\modset$ be a mod-set of some procedure $proc$.
Let $M$ and $\underbar{M}$ be two appropriate states for $\proc$.
Then, ${\pre{M}\oplus M \subseteq \modset^{\pre{M}}}$
iff $\pre{M}/M$ augmented with some interpretation for the function symbol
$\en{\modset}$ is a model of $\adapt{\modset}$.
\end{proposition}
