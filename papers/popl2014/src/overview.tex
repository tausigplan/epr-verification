\section{Adaptation of Local Effect to the Global Heap}
\label{sec:overview}
Our goal is to reason modularly about a procedure that modifies a subset of the heap.
We wish to automatically update the reachability relation in the entire heap based on the changes to
the modified subset.
We remark that in this paper we are concerned with reachability between any two nodes
in the heap, as opposed to only those pointed to by program variables.
When we discuss sharing we mean sharing via pointer fields in the heap
as opposed to aliasing from stack variables, which does not
concern us in this paper.

\subsection{Non-Local Effects}

Reachability is inherently non-local: a single edge
mutation can affect the reachability of an unbounded number of points that are an unbounded
distance from the point of change.
\figref{Find:scenario-nonlocal} contains a typical run of \tfind.
Two kinds of ``frames'' are depicted: (i)~$\modp=[x, r_x)_f$, specified by the programmer, denotes the nodes
whose edges can be directly changed by \tfind --- this is the standard notion of a frame condition;
(ii)~$\modprtc$ denotes nodes for which $f^*$, the path relation, has changed. We do not and, in
general we cannot, specify $\modprtc$ in a modular way because it usually depends on 
variables outside the scope of this function such as $y$ in \figref{Find:scenario-nonlocal}.
In the example shown, there is a path from $y$ to $c$ before the call which does not exist after the call.
Furthermore, $\modprtc$ can be an arbitrarily large set: in particular, it
may not be expressible as the union of a bounded set of intervals: for example, when adding a subtree as a child of some node in another tree,
$\modp$ spans only one edge, whereas $\modprtc$ is the entire subtree added ---
which may contain an unbounded number of branches.

\def\cw{3mm}

\begin{figure}
\centering
\begin{tikzpicture}
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode,join}, every join/.style=->]
        \node(x) {};
        \node(b) {};
        \node(c) {};
        \node(r) {};
      }
      \path(x) [late options={label={[name=xlbl]right:$x$}}]
           (c) [late options={label=right:$c$}];
      \node[listnode,left=6mm of x,label={[name=ylbl]left:$y$}](y) {};
      \node(z)[listnode,left=7mm of y,label=left:$z$] {};
      \node(zn)[listnode,above=of z] {}; \draw[->] (z) -> (zn);
      \node(modp)[modset,fit=(x) (r)] {};
      \node[modsetlbl,anchor=north east] at (modp.north west) {\small$\modp$};
      \draw[->] (y) -> (b);

      \node(modprtc)[frameset,fit=(modp) (y) (ylbl) (xlbl)] {};
      \node[framesetlbl,anchor=north east,inner sep=0] at (modprtc.north west) {\small$\modprtc$};

      \node[symbol,label=above:{\small\tfind\ $x$}] at(1.75,1) {$\leadsto$};

      % Now the same thing again to the right, with some edges relocated
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->]
        \node(x) at(5,0) {};
        \node(b) {};
        \node(c) {};
        \node(r) {};
      }
      \path(x) [late options={label={[name=xlbl]right:$x$}}]
           (c) [late options={label=right:$c$}];
      \node[listnode,left=6mm of x,label={[name=ylbl]left:$y$}](y) {};
      \node(z)[listnode,left=7mm of y,label=left:$z$] {};
      \node(zn)[listnode,above=of z] {}; \draw[->] (z) -> (zn);
      \node(modp)[modset,fit=(x) (r)] {};
      \node[modsetlbl,anchor=north east] at (modp.north west) {\small$\modp$};
      \draw[->] (y) -> (b);
      \draw[->] (c) -> (r);
      \draw[->] (x) edge[out=45,in=-45] (r);
      \draw[->] (b) edge[out=120,in=-135] (r);

      \node(modprtc)[frameset,fit=(modp) (y) (ylbl) (xlbl)] {};
      \node[framesetlbl,anchor=north east,inner sep=0] at (modprtc.north west) {\small$\modprtc$};
\end{tikzpicture}
\caption{\label{fi:Find:scenario-nonlocal}%
A case where changes made by \tfind\ have a non-local effect:
$\uprtc{y}{c}$, but $\lnot\prtc{y}{c}$.}
\end{figure}

The postcondition of \tfind\
is sound (every execution of \tfind\ satisfies it),
but incomplete: it does not provide a way to determine information
concerning paths outside $\modset$, such as from $y$ to $c$ in \figref{Find:scenario-nonlocal}.
Therefore, this rule is often not enough in order to verify the correctness of programs
that invoke \tfind\ in larger contexts.

Notice the difficulty of updating the global heap, especially the part $\modprtc\setminus\modp$.
In particular, using only the local specification of \tfind, one would
not be able to prove that $\lnot\frtc{y}{c}$.
Indeed, the problem is updating the reachability of elements that are {\bf outside} $\modset$;
in more complex situations, these elements may be far from the changed interval,
and their number may be unbounded.

One possibility to avoid the problem of incompleteness is to specify a postcondition which is specific to
the context in which the invocation occurs. However, such a solution requires reasoning per call site and
is thus {\em not modular}.
We wish to develop a rule that will fit in all contexts.
Reasoning about all contexts is naturally done by quantification.


\subsection{An FO(TC) Adaptation Rule}
\label{sec:adapt:fotc}


A standard way to modularize specifications is to specify the local effect of a procedure
and then to use a general adaptation rule (or frame rule) to derive the global effect.
In our case, we know that locations outside $\modset$ are not modified.
Therefore, for example, after a call to \tfind, a new path from node $\sigma$ to node $\tau$ is either an old path from $\sigma$ to $\tau$, or it
consists of an old path to a node $\alpha \in \modset$, a new path from $\alpha$ to a node $\beta
\in \modset$ and an old path from $\beta$ to $\tau$.  We express this below, first letting $q$
denote an old edge that is not inside mod:
%
\begin{equation}
  \begin{array}{rcl}
  \forall \alpha,\beta:  \redp{\alpha}{\beta} &\limpliesBidi& \upsingl{\alpha}{\beta} \land (\alpha\notin\modset \lor \beta\notin\modset)\\
  \forall \sigma,\tau:  \prtc\sigma\tau &\limpliesBidi& \redprtc{\sigma}{\tau} \slor \exists
  \alpha,\beta\in \modset:\\ 
      &&   \redprtc\sigma\alpha \sland \prtc\alpha\beta \sland \redprtc\beta\tau
  \end{array}
\label{eq:adapt:fotc}
\end{equation}

\equref{adapt:fotc} is a completely general adaptation rule: it defines $f^*$ on the
global heap assuming we know $f^*$ on the local heap and we also have access to the old path
relation $q^*$.
The problem with this rule is that it uses a logic that is too expressive and thus hard for
automated reasoning: $\FOTC$ is not decidable 
(in fact, not even recursively enumerable).
The first problem is that the $\redprtcSym$ relation is not usually first order expressible and generally requires transitive closure.
For example, \figref{NonUnique} shows that in general the adaptation rule is not necessarily
definable using only the reachability relation, when there are multiple
outgoing edges per node. We avoid this problem by only reasoning about functional fields, $f$.

\begin{figure}
\centering
\begin{tikzpicture}[h/.style={fill=blue!10}]
    \def\cw{3mm}
    % (a)
    \begin{scope}[local bounding box=sa]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode  ](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode,h](t3) {\tiny 3}; & \node[listnode,h](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2); \draw[->] (t2) -- (t3);
    \draw[->] (a) edge[out=60] (t2);
    \end{scope}
    % (b)
    \begin{scope}[xshift=4cm,local bounding box=sb]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode  ](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode,h](t3) {\tiny 3}; & \node[listnode,h](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2); \draw[->] (t2) -- (t3);
    \draw[->] (a) edge[out=60] (t2);
    \draw[->] (a) edge[out=-60, in=-135] (t3);
    \end{scope}
    % (c)
    \begin{scope}[yshift=-2cm,local bounding box=sc]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode](t3) {\tiny 3}; & \node[listnode](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2);
    \draw[->] (a) edge[out=60] (t2);
    \end{scope}
    % (d)
    \begin{scope}[xshift=4cm,yshift=-2cm,local bounding box=sd]
    \matrix[column sep=2mm] {
        \node[listnode,h,label=left:$\alpha$](a) {}; & &
        \node[listnode  ](t1) {\tiny 1}; & \node[listnode,h](t2) {\tiny 2}; &
        \node[listnode,h](t3) {\tiny 3}; & \node[listnode,h](t4) {\tiny 4}; \\
    };
    \draw[->] (t3) -- (t4);
    \draw[->] (t1) -- (t2);
    \draw[->] (a) edge[out=60] (t2);
    \draw[->] (a) edge[out=-60, in=-135] (t3);
    \end{scope}
    % Do all subfigure labels
    \foreach \x in {a,b}
       { \node[above=0mm of s\x] {\small(\x)}; }
    \foreach \x in {c,d}
       { \node[below=0mm of s\x |- sd.south] {\small(\x)}; }
    % Edges (a)-(c)  (b)-(d)
    \node(l1)[coordinate] at ($(sa |- sb.south)!.5!(sc.north)$) {};
    \draw[->] ($(sa.south |- sb.south)+(5mm,0)$) to[out=-60,in=60] ($(l1)+(5mm,0)$) to[out=-120,in=120]
            ($(sc.north)+(5mm,0)$);
    \node[right=5mm of l1] {\small{\tt del} $\abra{2,3}$};
    \node(l1)[coordinate] at ($(sb.south)!.5!(sd.north)$) {};
    \draw[->] ($(sb.south)+(5mm,0)$) to[out=-60,in=60] ($(l1)+(5mm,0)$) to[out=-120,in=120]
            ($(sd.north)+(5mm,0)$);
    \node[right=5mm of l1] {\small{\tt del} $\abra{2,3}$};
\end{tikzpicture}
\caption{\label{fi:NonUnique}%
Memory states with non-unique pointers where global reasoning about reachability is hard.
In the memory state~(a), there is one edge from $\alpha$ into the modified-set $\{1, 2, 3, 4\}$,
and in memory state~(b), there are two edges from $\alpha$ into the same modified-set, $\{1, 2, 3, 4\}$.
The two memory states have the same reachability relation and therefore are indistinguishable in terms of reachability.
The memory states~(c)  and (d) are obtained from the memory states (a) and (b), respectively, by deleting the edge $\langle 2, 3\rangle$.
The reachability in (c) is not the same as in (d), which shows it is impossible to update
reachability in general w.r.t. edge deletion, without using the edge relation.}
\end{figure}

The second problem with \equref{adapt:fotc} is that it contains quantifier alternation.
$\alpha$ matches an arbitrary node in $\modset$ which may be of
arbitrary size.
Therefore, it is not completely obvious how to avoid existential quantifications.

\subsection{An Adaptation Rule in a Restricted Logic}
\label{sec:adapt:afar}

\begin{figure}
\centering
\begin{tikzpicture}[en/.style={-stealth,dashed,shorten <=1mm,shorten >=1mm,draw=black!50,looseness=1.3}]
    \def\cw{4mm}
    \begin{scope}[start chain,node distance=\cw,a/.style={on chain,join},every join/.style={->}]
        \node(delta)[a,listnode] {};
        \node(spc1)[a,listnode] {};
        \node(alpha)[a,listnode] {};
        \node(beta)[a,listnode] {};
        \node(dummy)[a] {\ldots};
    \end{scope}
    \node(modp)[fit=(alpha) (dummy),modset,label={[modsetlbl]below:\small$\modset$}] {};
    \node(eps)[listnode,above=\cw of spc1] {};
    \node(spc2)[listnode,right=\cw of eps] {};
    \draw[->] (eps) -> (spc2); \draw[->] (spc2) -> (beta);
    \draw[en] (delta) to[out=-70,in=-110] node[below] {\small $\en{\modset}$} (alpha);
    \draw[en] (eps) to[out=40,in=80] node[above right] {\small $\en{\modset}$} (beta);
    \path[en,looseness=12] (alpha) edge[out=105,in=80] (alpha)
      (beta) edge[out=60,in=30] (beta);
    \node[right=0.3 of modp,align=left]
        {\small $\en{\modset} : V \to\modset$};
\end{tikzpicture}
\caption{\label{fi:en-modp}
The function $\en{\modset}$ maps every node $\sigma$ to
the first node in $\modset$ reachable from $\sigma$. Notice that
for any $\alpha\in\modset$, $\en{\modset}(\alpha)=\alpha$ by definition.}
\end{figure}



We now present an equivalent adaptation rule in a restricted logic,
without transitive closure or extra quantifier-alternations.
This is possible due to our assumptions from \secref{working-assumptions} and
it greatly simplifies reasoning about modified paths in the entire heap.
We require a new function symbol, $\en{\modset}$. We call $\en{\modset}(\sigma)$ the \textbf{entry
  point} of $\sigma$ in $\modset$, 
i.e., the first node on the (unique) path from $\sigma$ that enters $\modset$, and
$\vnull$ if no such node exists (see Figure \ref{fi:en-modp}).

Note that since transitive closure is only applied to functions, entry points such as $\alpha$ in \equref{adapt:fotc} are uniquely determined by $\sigma$, the origin of the path.
A key property of $\en{\modset}$ is that on $\modset$ itself, $\en{\modset}$ is the identity, and therefore for any $\sigma\in V$ it holds that
$\en{\modset}(\en{\modset}(\sigma)) = \en{\modset}(\sigma))$ ---
that is, the function $\en{\modset}$ is \newterm{idempotent}.
It is important to note
that $\en{\modset}$ does not change as a result of local modifications in $\modset$. Hence, we do not need to worry about $\en{\modset}$ in the pre-state as opposed to the post-state.
Formally, $\en{\modset}$ is characterized by the following formula:

\begin{equation}
\begin{array}{ll}
\forall \sigma: &(\en{\modset}(\sigma) = \vnull \land \forall \alpha \in \modset: \lnot
\uprtc{\sigma}{\alpha})\; \lor\\
&(\uprtc{\sigma}{\en{\modset}(\sigma)} \land
\en{\modset}(\sigma) \in \modset \; \land \\
& \forall \alpha \in \modset: \uprtc{\sigma}{\alpha} \limplies \uprtc{\en{\modset}(\sigma)}
{\alpha})
\end{array}
\label{eq:enmodp:axiom}
\end{equation}

Using $\en{\modset}$ the new
adaptation rule $\adapt{\modset}$ is obtained by considering, for every source and target, the following
three cases:

\begin{tabular}{rl}
{\bf Out-In:} & The source is out of $\modset$; the target is in;\\
{\bf In-Out:} &The source is in $\modset$; the target is out;\\
{\bf Out-Out:} & The source and target are both out of $\modset$.
\end{tabular}

The full adaptation rule is obtained by taking the conjunction of the
formulas for each case (\equref{out-in adaptation},
\equref{in-out adaptation}, \equref{out-out adaptation}),
that are described below, and the formula defining $\en{\modset}$ 
(\equref{enmodp:axiom}).

\paragraph{Out-In Paths}

Using $\en{\modset}$ we can easily handle
paths that \emph{enter} $\modset$.
Such paths originate at some $\sigma\notin\modset$ and terminate
at some $\tau\in\modset$. Any such path therefore has to go through $\en{\modset}(\sigma)$ as depicted in \figref{adapt:depiction}.
Thus, the following simple formula can be used:

\begin{equation}
\label{eq:out-in adaptation}
\forall \sigma\notin\modset,\tau\in\modset:
            \prtc\sigma\tau \liff \prtc{\en{\modset}(\sigma)}{\tau}
\end{equation}

Observe that for any $\beta\in\modset$, the atomic formula used above, $\prtc{\en{\modset}(\sigma)}{\beta}$, corresponds to the FO(TC) sub-formula
$\exists\alpha \in \modset: \redprtc{\sigma}{\alpha}\land\prtc{\alpha}{\beta}$
from \equref{adapt:fotc}.

\begin{figure}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \node[listnode,label=above:$\sigma$](delta) {};
    \matrix(modp)[right=of delta, column sep=7mm, row sep=0mm] {
        \node(alpha)[listnode,label=above:$\alpha$] {}; & &
        \node(t1)[listnode,label=above:$t_1$] {}; \\
        & \node(beta)[listnode] {}; & \\
        & & \node(t2)[listnode,label=above:$t_2$]{}; \\
    };
    \node[modset,fit=(modp),label={[modsetlbl]below:$\modset$}] {};
    \node(epsilon)[listnode,right=of modp.-23,label=above:$\tau$] {};
    \draw[->,extpath] (delta) to[out=0,in=180] (alpha);
    \draw[->] (alpha) to[out=30,in=90] (beta);
    \draw[->] (beta) to[out=-90,in=-180] (t2);
    \draw[->,extpath] (t2) to[out=0,in=180] (epsilon);
    \node(dummy)[right=of t1] {\ldots};
    \draw[->,extpath] (t1) -- (dummy);
\end{tikzpicture}
\caption{\label{fi:adapt:depiction}%
This diagram depicts how an arbitrary path from $\sigma\notin\modset$
to $\tau\notin\modset$ is constructed from three segments:
$[\sigma,\alpha]_{\upsinglSym}$, $[\alpha,t_i]_f$, and
$[t_i,\tau]_{\upsinglSym}$ (here $i=2$).
Arrows in the diagram denote paths;
thick arrows entering and exiting the box denote paths that were not modified since they are outside of $\modset$.
Here, $\alpha=\en{\modset}(\sigma)$ is an entry-point and $t_1, t_2$ are exit-points.}
\end{figure}

\paragraph{In-Out Paths}

We now shift attention to paths that \emph{exit} $\modset$.
Exit points, that is, last points on some path that belong to $\modset$, are more subtle since both ends of the path are needed to determine them.
The end of the path is not enough since it can be shared,
and the origin of the path is not enough
since it can exit the set multiple times, because a path may exit $\modset$ and enter it again later.
Therefore, we cannot define a function in a similar manner to $\en{\modset}$.
The fact that transitive closure is only applied to functions is useful here: every interval $[\alpha, \beta]$ has at most one exit $\beta$.
We therefore utilize the fact that $\modset$ is expressed as a bounded union of
intervals --- which bounds the potential exit points to a bounded set of terms.
We will denote the exit points of $\modset$ by $t_i$.

\begin{figure}
\begin{tabular}{@{}l@{ }l@{}}
@ requires & $E_f(x,f^1_x) \land E_f(f^1_x,f^2_x) \land E_f(f^3_x,f^2_x) \land $ \\
& $x \neq \vnull \land 
f^1_x \neq \vnull \land f^2_x \neq \vnull$ \\
@ $\modset$ & $[x,f^3_x]$\\
@ ensures &  \ldots
\end{tabular}
\begin{minipage}[t][1.8cm][b]{2cm}
{\alltt
\begin{tabbing}
vo\=id swap(Node x) \{\+\\
Node t = x.f;\\
x.f = t.f;\\
t.f = x.f.f;\\
x.f.f = t;\-\\
\}
\end{tabbing}
}
\end{minipage}
%
\def\cw{3.5mm}
\def\dw{4.5mm}
\begin{tikzpicture}[sll chain,k/.style={listnode,on chain},baseline=(modn)]
    \node(h)[k] {};
    \node(x)[k,label=above:\small$x$] {};
    \node(t)[k,join,label=below:\small$t$] {};
    \node(c)[k,join] {};
    \node(d)[k,join,label=above:\small{$f^3_x$}] {};
    \node(out)[k] {};
    \begin{scope}[local bounding box=ub]
    \path[-stealth,thick, dotted] (x) edge[out=45,in=120] (c)
        (c) edge[out=140,in=35] (t) (t) edge[out=-45,in=-135] (d);
    \end{scope}
    \node(modn)[modset,fit=(x) (t) (c) (d) (ub),inner xsep=0.5*\dw] {};
    \path[->] (h) edge (x) (d) edge (out);
    \node[modsetlbl,anchor=north east] at (modn.south east) {$\modn$};
\end{tikzpicture}
\caption{\label{fi:Swap}%
A simple function that swaps two adjacent elements following {\tt x} in a singly-linked list. 
Dotted lines denote the new state after the swap.
The notation e.g. $E_f(x,f_x^1)$ denotes the single edge from $x$
to $f_x^1$ following the $f$ field.}
\end{figure}

For example, in the procedure \tswap\ shown in \figref{Swap}, $\modset = [x, f^3_x]$ and there is one exit point $t_1=f^3_x$
($f^3_x$ is a constant set by the precondition to have the value of $f(f(f(x)))$ using
the inversion formula \equref{Inversion} to be introduced formally in \secref{decidability-and-inversion}).

Any path that originates in $\modset$
and terminates outside $\modset$ must leave through a last exit point $t_i$ (see
\figref{adapt:non-convex}). 
Notice that the exit points also do not change as a result of modifying edges between nodes in $\modset$.
Let $p$ be a path from
$\sigma$ to $\tau$ and let $t_i$ be the last exit point along $p$.
Note that the part of the path from 
$t_i$ to $\tau$ consists only of unchanged edges ---
since they are all outside of $\modset$.
We can therefore safely use $\uprtcSym$, rather than $\redprtcSym$, to characterize it.
The part of the path from $\sigma$ to $t_i$ can be characterized by $\prtcSym$,
because $\sigma$ and $t_i$ are both in $\modset$.
Therefore the entire path can be expressed as $\prtc{\sigma}{t_i}\land \uprtc{t_i}\tau$. Thus, we obtain the following formula:

\begin{equation}
\label{eq:in-out adaptation}
\begin{array}{l}
\forall \sigma\in\modset,\tau\notin\modset: \; \prtc\sigma\tau \;\liff\\
\hspace*{.3in}\bigvee_{t_i} (\prtc\sigma{t_i} \land \uprtc{t_i}\tau \;\land \;
\bigwedge_{t_j\neq t_i} t_j\notin[t_i,\tau]_{\upsinglSym})
\end{array}
\end{equation}

%We observe that in practice, most mod-sets are \newterm{convex},
%i.e., for every $\alpha,\beta\in\modset$, $[\alpha,\beta]_f\subseteq\modset$.
%This makes sure that every path has at most one exit point. In fact, if we assume %this, \equref{in-out adaptation} can be shortened to:
%\begin{equation}
%\begin{array}{l}
%\forall \sigma\in\modset,\tau\notin\modset: \\ \prtc\sigma\tau \liff
%\bigvee_{t_i} \prtc\sigma{t_i} \land \uprtc{t_i}\tau
%\end{array}
%\end{equation}
%Non-convex sets are rare in practice; for example, they occur if the same procedure
%manipulates several segments of the same list that are far apart.
%In such cases, we still need to assert that $t_i$ is the last exit point on the path from $\alpha$ to $\tau$ (see \figref{adapt:non-convex};
%see for example the code in \figref{swap_two:code}).
%For this, it is enough to require that $[t_i,\tau]_{\upsinglSym}$ contains no
%other exit points. Let $t_j$ be some other exit point,
%then checking $t_j\not\in[t_i,\tau]_{\upsinglSym}$ is a quantifier-free
%formula using the symbol $\uprtcSym$, which we are free to use with any
%pair of nodes.

Note that \equref{in-out adaptation}
corresponds the sub-formula $\exists \beta: \prtc\alpha\beta \land \redprtc\beta\tau$
in \equref{adapt:fotc}.

\begin{figure}
\centering
\begin{tikzpicture}[nose/.style={minimum size=0.5mm,inner sep=0mm}]
    \def\cw{3mm}
    \matrix[column sep=8mm] {
        \node(delta)[listnode,label=above:$\sigma$] {}; &
        \node(en1)[nose] {}; &
        \node(t1)[listnode,label={[name=t1lbl]above:$t_1$}]  {}; &
        \node(beta)[listnode,label=above:$\tau_1$] {}; &
        \node(en2)[nose] {}; &
        \node(t2)[listnode,label={[name=t2lbl]above:$t_2$}] {}; &
        \node(epsilon)[listnode,label=above:$\tau_2$] {}; \\
    };
    \node[modset,fit=(en1) (t1) (t1lbl)](mod1) {};
    \node[modset,fit=(en2) (t2) (t2lbl)](mod2) {};
    \draw[->,extpath] (delta) -> (en1);
    \draw[->] (en1) -- (t1);
    \draw[->,extpath] (t1) -> (beta);
    \draw[->,extpath] (beta) -> (en2);
    \draw[->] (en2) -> (t2);
    \draw[->,extpath] (t2) -> (epsilon);
    \draw[modset,double] (mod1) to[out=-45,in=-135] node[above,modsetlbl] {\small$\modset$} (mod2);
\end{tikzpicture}
\caption{\label{fi:adapt:non-convex}%
A subtle situation occurs when the path from $\sigma$
passes through multiple exit-points. In such a case,
the relevant exit-point for $\prtc\sigma{\tau_1}$ is $t_1$,
whereas for $\prtc\sigma{\tau_2}$ and $\prtc{\tau_1}{\tau_2}$
it would be $t_2$.}
\end{figure}

\begin{figure}
\begin{alltt}
\begin{tabbing}
vo\=id swap_two(Node a, Node b) \{\+\\
swap(a); swap(b);\-\\
\}
\end{tabbing}
\end{alltt}
\caption{\label{fi:swap_two:code}%
An example of a procedure where the mod-set is not (essentially) convex.}
\end{figure}

%In our preliminary experience, most naturally occurring procedures fulfil this requirement.
%An artificial program with unbounded number of intervals is given in the appendix.
%This program manipulates the beginning of lists nested in an arbitrary-length outer list.


\paragraph{Out-Out Paths}

For paths between $\sigma$ and $\tau$, both outside $\modset$, there are two
possible situations:
\begin{itemize}
  \item The path goes {\bf through} $\modset$ (as in \figref{adapt:depiction}).
In this case, we can reuse the in-out case, by taking $\en{\modset}(\sigma)$
instead of $\sigma$.
  \item The path is entirely outside of $\modset$ (see \figref{adapt:outside}).
\end{itemize}

The corresponding formula in this case is:

\begin{equation}
\label{eq:out-out adaptation}
\begin{array}{l}
\forall \sigma\notin\modset,\tau\notin\modset:\; \prtc\sigma\tau\; \liff\\
\hspace*{.1in}\bigvee_{t_i}(\prtc{\en{\modset}(\sigma)}{t_i} \land \uprtc{t_i}\tau \land \;
\bigwedge_{t_j\neq t_i} t_j\notin[t_i,\tau]_{\upsinglSym})\\
\hspace*{.1in} \lor \quad            \en{\modset}(\sigma) = \en{\modset}(\tau) \;\land\; \uprtc{\sigma}{\tau}
\end{array}
\end{equation}

Notice that the second disjunct covers the case where there is a path from $\tau$ to $\modset$ 
($\en{\modset}(\sigma)= \en{\modset}(\tau)\ne \vnull$) and the case where there is none
($\en{\modset}(\sigma)= \en{\modset}(\tau)= \vnull$). 

\begin{figure}
\centering
\begin{tikzpicture}
    \def\cw{4mm}
    \begin{scope}[start chain,every node/.style={on chain,listnode},every join/.style={->,extpath},node distance=10mm]
        \node(delta1) {};
        \node(epsilon1)[join] {};
        \node(alpha) {};
    \end{scope}
    \begin{scope}[yshift=-1cm,start chain,every node/.style={on chain},node distance=14mm]
        \node(delta2)[listnode] {};
        \node(epsilon2)[listnode,join={by ->,extpath}] {};
        \node(nul)[join=by extpath,yscale=.7] {};
    \end{scope}
    %\draw (epsilon2) to[extpath,ground={pos=1},red] (nul);
    % Put labels (must do that outside chain scope)
    \path (delta1) [late options={label=above:$\sigma_1$}]
        (epsilon1) [late options={label=above:$\tau_1$}]
        (alpha) [late options={label={[name=alphalbl]above:$\alpha$}}]
        (delta2) [late options={label=below:$\sigma_2$}]
        (epsilon2) [late options={label=below:$\tau_2$}];
    % Put alpha in a modset frame
    \node(modp)[fit=(alpha) (alphalbl) ($(alpha)+(right:1cm)$),label={[modsetlbl]below:\small$\modset$}] {};
    \path[modset] (modp.north east) -- (modp.north west) -- (modp.south west) -- (modp.south east);
    \draw[extpath,->] (epsilon1) -> (alpha);
    % Make an adorable, cute ``ground'' symbol
    \foreach \x/\y in {0.1/0, 0.55/0.2, 1/0.4} {
        \node(u)[coordinate] at ($(nul.north west)!\x!(nul.north east)$) {};
        \node(v)[coordinate] at ($(nul.south west)!\x!(nul.south east)$) {};
        \draw[extpath] ($(u)!\y!(v)$) -- ($(v)!\y!(u)$);
    }
\end{tikzpicture}
\caption{\label{fi:adapt:outside}%
Paths that go entirely untouched. $\en{\modset}(\sigma_1)=\alpha$,
whereas $\en{\modset}(\sigma_2)=\vnull$.}
\end{figure}

\bigskip
In conclusion, our adaptation rule, $\textrm{adapt}[\modset]$, is the conjunction
of the three formulas in \equref{out-in adaptation},
\equref{in-out adaptation}, \equref{out-out adaptation}, and the formula defining $\en{\modset}$ 
(\equref{enmodp:axiom}). 
We need some more formalism, introduced in the next section, before we show 
that $\textrm{adapt}[\modset]$ meets our needs.


