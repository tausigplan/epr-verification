\section{Generating Verification Condition for Procedure With Sub-calls in $\AEAR$}
\label{sec:VC}

We follow the standard procedures, e.g.~\cite{Book:Winskel1993}, which generates a verification condition for a procedure annotated with specification using weakest (liberal) preconditions. Roughly speaking, for a Hoare triple $\{P\} \prog \{Q\}$,
we generate the usual verification condition
$\vc{\prog} = P \limplies \wlp{\prog}(Q)$.

For the basic commands (assignment, composition, and conditional) we employ  the standard definitions, given in 
\tabref{Wprules}.

 \begin{table}
\[\begin{array}{@{}c@{}}
 \begin{array}{|rcl|}
 \hline
 \wlp{\skipc}(Q) &\eqdef& Q\\
 \wlp{x := y}, Q) &\eqdef& \substitute{Q}{x}{y}\\
\wlp{S_1 ~; ~ S_2}(Q) &\eqdef& \wlp{S_1}\big(\wlp{S_2}(Q)\big)\\
\wlp{\IF ~ B ~ \THEN~ S_1  ~ \ELSE~ S_2}(Q) &\eqdef&
\semp{B}\land \wlp{S_1}(Q)\;\lor \\ &&\lnot\semp{B}\land\wlp{S_2}(Q)\\
\hline
\end{array}
 \end{array}\]
\caption{\label{ta:Wprules}%
Standard rules for computing weakest liberal preconditions for loop free code without pointer accesses.
$\semp{B}$ is the \AFR\ formula for program conditions and $Q$ is the postcondition expressed as an \AFR\ formula.}
\end{table}


\subsection{Modular Verification Conditions}


The modular verification condition would also contain a conjunct for checking
that $\modset$ affected by the invoked procedure is a subset of the
``outer'' $\modset$. This way the specified restriction can be checked
in $\AEAR$ and the SMT solver can therefore be used to enforce it automatically.

\subsection{Weakest-precondition of Call Statements}

As discussed in \secref{overview}, the specification as it appears in the ``ensures''
clause of a procedure's contract is a local one, and in order to make reasoning
complete we need to adapt it in order to handle arbitrary contexts.
This is done by conjoining $Q_{\proc}$ occurring in the ``ensures'' clause
from the specification
of $\proc$ with the universal adaptation rule $\textrm{adapt}[\modset]$, where $\modset$ is replaced with the mod-set
as specified in the ``modifies'' clause of $\proc$.

\tabref{wp:call-statement} presents a formula for the weakest-precondition
of a statement containing the single procedure call, where the invoked
procedure has the specifications as in \figref{proc:spec},
where ``$\proc$'' has the formal parameters $\vecx=x_1,\ldots,x_k$,
and it is used with $\veca=a_1,\ldots,a_k$
(used in the formula)
as the actual arguments for a specific procedure call;
we assume w.l.g. that each $a_i$ is a local variable of the calling procedure.

\begin{figure}
\begin{tabular}{@{}l@{ }l@{}l}
@ requires & $P_{\proc}$ \\
@ $\modset$ & $\modset_{\proc}$ \\
@ ensures & $Q_{\proc}$ \\
\end{tabular}\\
\textit{return-type} $\proc(\vecx)$ \texttt{\{ \ldots\ \}}
\caption{\label{fi:proc:spec}%
Specification of $\proc$ with placeholders.}
\end{figure}

\begin{table}
\renewcommand{\arraystretch}{1.2}
\[
    \begin{array}{l@{\ }l}
        \wlp{r := &proc(\overline{a})}(Q) \eqdefS \\
        &\subst{P_{\proc}}{\vecx}{\veca}
            \;\land\\
        & \forall \alpha: \alpha\in \subst{\modset_{\proc}}{\vecx}{\veca} \limplies \alpha\in \modset_{\prog} \;\land \\
        &
        \forall \zeta: \substFOUR{Q_{\proc}}{\vecx}{\veca}{f}{\freshfSym}{\ufsinglSym}{f}{\retval}{\zeta}
        \land \\
        &\quad \substTHREE{\textrm{adapt}\left[{\subst{\modset_{\proc}}{\ufsinglSym}{f}}\right]}
        	{\vecx}{\veca}{f}{\freshfSym}{\ufsinglSym}{f}
        \limplies \\
        &\quad \substTWO{Q}{f}{\freshfSym}{r}{\zeta}
    \end{array}
\]
\caption{\label{ta:wp:call-statement}%
Computing the weakest (liberal) precondition for a statement containing
a procedure call. $r$ is a local variable that is assigned the return value;
$\veca$ are the actual arguments passed. {\protect\freshfSym} is a fresh function
symbol.}
\end{table}

In general it is not obvious how to enforce that the set of locations
modified by inner calls is a subset of the set of locations declared
by the outer procedure. Moreover, this can be tricky to check since
it depends on aliasing and paths between nodes pointed to by different variables.
Fortunately,
the sub-formula $\forall \alpha: \alpha\in \subst{\modset_{\proc}}{\vecx}{\veca} \limplies \alpha\in \modset_{\prog}$
captures this property, ensuring that the outer procedure does not exceed its own $\modset$
specification, taking advantage of the interval-union structure of the $\modset$. 
Since all the modifications (even atomic ones)
are done by means of procedure calls, this ensures that no edges incident to
nodes outside $\modset$ are changed.

\newcommand\uM{\underline{M}}

\begin{proposition}
The rule for $\wlp{}$ of call statements is sound
and complete, that is, when $\proc$ is a procedure with specification
as in \figref{proc:spec}, called in the context of $\prog$ whose mod-set is $\modset$:
\begin{equation}
	\begin{array}{c}
		\uM \models \wlp{r := \proc(\veca)}(Q) \\
		\Updownarrow \\
		\uM\models \subst{P_{\proc}}{\vecx}{\veca} \land 
			\modset^{\uM}_{\proc} \subseteq \modset^{\uM} \land \\
		\forall M: \big(
			\uM/M\models \subst{Q_{\proc}}{\vecx}{\veca}
			\land \uM\oplus M\subseteq \modset^{\uM}_{\proc}\big)\\
			\Rightarrow
			M[r\mapsto \retval^M] \models Q
	\end{array}
\end{equation}
\end{proposition}

\subsection{Reducing Function Symbols}
\label{sec:Reducing}

Notice that when we apply the adaptation rule for $\AEAR$, as discussed
above, it introduces a new function symbol $\en{\modset}$
depending on the concrete mod-set of the called procedure.
This introduces a complication:
the mod-sets of separate procedure calls may differ from the one of the top procedure, hence multiple applications
of \tabref{wp:call-statement}
naturally require a separate function symbol $\en{\modset}$ 
for every such invocation.
Consider for example the situation of \tunion\ making two invocations
to \tfind. In \figref{modular:find-in-union} one can see that
the $\modset$ of \tunion\ is $[x,r_x]_f\cup [y,r_y]_f$, while the
$\modset$ of the first call \texttt{t := find(x)} is $[x,r_x]_f$, which
may be a proper subset of the former. The $\modset$ of the second
invocation is $[y,r_y]_f$, which may overlap with $[x,r_x]_f$.


\begin{figure}
\centering
\def\cw{4mm}
\begin{tikzpicture}[decoration={snake,amplitude=1mm,segment length=14mm,pre length=1mm,post length=1mm},
    en/.style={-stealth,dashed,shorten <=1mm,shorten >=1mm,draw=black!50,looseness=1.3}]
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(x) {5};
        \node(b)[join] {4};
        \node(c)[join] {3};
        \node(d)[join] {2};
        \node(r)[join] {1};
      }
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(y)[left=0.5cm of x] {5};
        \node(e)[join] {4} edge[->] (c);
      }
      \path[label distance=1.5mm, inner xsep=0]
            (x) [late options={label={[name=xlbl]right:$x$}}]
            (y) [late options={label={[name=ylbl]left:$y$}}];
      \begin{pgfonlayer}{background layer}
      \node(inner0)[inner modset,fit=(x) (xlbl) (r),inner ysep=2mm,inner xsep=2.5mm] {};
      \end{pgfonlayer}
      \node(inner lbl)[inner modsetlbl,anchor=north east] at (inner0.north west) {\small{$[x,r_x]_f$}};

      \node(outer)[modset,fit=(inner0) (inner lbl) (x) (xlbl) (y) (ylbl),inner xsep=1.5mm] {};
      \node[modsetlbl,anchor=south west,align=right]
         at (outer.north west) {\small{$[x,r_x]_f \cup [y,r_y]_f$}};

      \path[en] (y) edge[out=150,in=170]
        node[pos=0.8,anchor=south east,inner sep=.5mm] {\tiny{$\en{[x,r_x]}$}} (c);

      \begin{scope}[xshift=5cm]
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(x) {5};
        \node(b)[] {4};
        \node(c)[] {3};
        \node(d)[] {2};
        \node(r)[join] {1};
      }
      { [start chain=going above, node distance=4mm, every node/.style={on chain,listnode}, every join/.style=->, font=\tiny]
        \node(y)[left=0.5cm of x] {5};
        \node(e)[join] {4} edge[->] (c);
      }
      \path[->] (c) edge[out=120,in=-120] (r)
                (b) edge[out=65,in=-65] (r)
                (x) edge[out=60,in=-46,looseness=.7] (r);
      \path[label distance=1.5mm, inner xsep=0]
            (x) [late options={label={[name=xlbl]right:$x$}}]
            (y) [late options={label={[name=ylbl]left:$y$}}]
            (r) [late options={label={[name=tlbl]right:$t$}}];
      \begin{pgfonlayer}{background layer}
      \node(inner)[inner modset,fit=(x) (xlbl) (r),inner ysep=2mm,inner xsep=2.5mm] {};
      \end{pgfonlayer}
      \node(inner lbl)[inner modsetlbl,anchor=north east] at (inner.north west) {\small{$[x,r_x]_{\upsinglSym}$}};

      \node(outer)[modset,fit=(inner) (inner lbl) (x) (xlbl)] {};
      \node[modsetlbl,anchor=south west,align=right]
         at (outer.north west) {\small{$[x,r_x]_{\upsinglSym} \cup [y,r_y]_{\upsinglSym}$}};

      \path[en] (y) edge[out=150,in=170]
        node[pos=0.8,anchor=south east,inner sep=.5mm] {\tiny{$\en{[x,r_x]}$}} (c);

      \end{scope}

      \path[->] (inner0.50) edge[shorten <=1mm,shorten >=1mm,decorate]
        node[pos=0.33,above=3mm] {\small{$t$ := \tfind\ $x$}}
       (inner.130) ;
\end{tikzpicture}
\caption{\label{fi:modular:find-in-union}%
An example invocation of \tfind\ inside \tunion.}
\end{figure}

To meet the requirement of $\AEAR$ concerning the function symbols, we observe that:
$(a)$ the amount of sharing 
any particular function call creates, as well as the entire call,
is bounded, and we can pre-determine a bound for it;
$(b)$ the modification set of any sub-call must be a subset
of the top call, as otherwise it violates the obligation not to change any edge outside $\modset$.
These two properties allow us to express the functions $\en{\modset}$ of the
sub-calls using $\en{\modset}$ of the top procedure and extra intermediate functions with bounded image.
Thus, we replace all of the function symbols
$\en{S}$ introduced by $\adapt{S}$ for different $S$'s, with a single
(global, idempotent) function symbol together with a set of bounded function
symbols.

Consider a statement $r := \proc(\veca)$ in
a procedure $\prog$. Let $A$ denote the mod-set of
$\prog$, and $B$ --- the mod-set of $\proc$.
We show how $\en{B}$ can be expressed using $\en{A}$ and one more function,
where the latter has a bounded range.
We define $\en{B|A} : A\setminus B \to B$ a new function that is the
restriction of $\en{B}$ to the domain $A\setminus B$.
$\en{B|A}$ is defined as follows:
%
\begin{equation}
\en{B}(\sigma)\eqdefS
\begin{cases}
\en{A}(\sigma) & \en{A}(\sigma)\in B \\
\en{B|A}(\en{A}(\sigma)) & \text{otherwise} \\
\end{cases}
\end{equation}

Using equality the nesting of function symbols can be reduced (without affecting
the quantifier alternation).

Consult \figref{en-modp:inner}; notice that
$\en{B|A}(\sigma)$ is always either:
\begin{itemize}
  \item The beginning $s_i$ of one of the intervals $[s_i,t_i]_f$ of $B$
    (such as \tikz[baseline=(a.base)] \node(a)[listnode] {\tiny 1}; in the figure);
  \item A node that is {\bf shared} by backbone pointers from two nodes
     in $A$ (such as \tikz[baseline=(a.base)] \node(a)[listnode] {\tiny 3};);
  \item The value $\vnull$.
\end{itemize}

\begin{figure}
\centering
\begin{tikzpicture}[k/.style={listnode,on chain},entry function]
    \def\cw{4mm}
    \def\dw{6mm}
    \begin{scope}[sll chain]
        \node(d1)[k] {};
        \node(d2)[k,join] {};
        \node(d3)[k,join] {};
    \end{scope}
    \begin{scope}[sll chain,yshift=-10mm,xshift=-5mm]
        \node(e0)[k] {};
        \node(e1)[k] {\tiny 1};
        \node(e2)[k,join] {\tiny 2};
        \node(e3)[k,join] {\tiny 3};
        \node(e4)[k,join] {\tiny 4};
    \end{scope}
    \draw[->] (d3) -> (e3); \draw[->] (e0) -> (e1);
    \node(inner modp)[inner modset,fit=(e1) (e2) (e3) (e4), inner sep=0.7*\cw] {};
    \node(lbl)[inner modsetlbl,anchor=north east,inner sep=0.5mm] at (inner modp.south east) {\small(inner)$\modset=B$};
    \node(modp)[modset,fit=(d1) (d2) (d3) (e0) (inner modp), inner sep=\dw] {};
    \node[modsetlbl,anchor=south east] at (modp.north east) {\small$\modset=A$};

    % enmodp and en-> edges
    \node(delta)[listnode,label=left:$\delta$,above=of d1] {}
         edge[out=0,in=90,en] node[above right]{$\en{A}$} (d2);
    \path (d2) edge[en,out=45,in=55,looseness=2] node[right=2mm]{$\en{B|A}$} (e3);
\end{tikzpicture}
\caption{\label{fi:en-modp:inner}%
The inner $\en{\modset}$ is constructed from the outer one by composing with
an auxiliary function $\en{B|A}$.}
\end{figure}

A bound on the number of $s_i$'s is given in the modular specification
of $\proc$. A bound on the number of shared nodes is given in the next subsection.
This bound is effective for all the procedure calls in $\prog$;
hence $\en{B|A}$ can be used in the restricted logic $\AEAR$.

\subsection{Bounding the Amount of Sharing}
\label{sec:bounding-sharing}

We show that under the restrictions of the specification
given in \secref{overview} and \secref{modular-spec}, the number of shared
nodes inside $\modset$ --- that is, nodes in $\modset$ that are pointed to by more than one
$f$-pointer of other nodes in $\modset$ --- has a fixed bound throughout
the procedure's execution.

Consider a typical loop-free program $\prog$ containing calls 
of the form $v_i$ := $\proc_i$($\veca_i$).
Assume that the mod-set of $\prog$ is a union of $k$ intervals.
We show how to compute a bound on the number of shared nodes inside
the mod-set. Since there are $k$ start points,
at most $\binom{k}{2}$ elements can be shared when
$\prog$ starts executing. Each sub-procedure invoked from $\prog$ may
introduce, by our restriction, at most as many shared nodes as there 
are local variables in the sub-procedure. Therefore, by computing the
sum over all invocation statements in $\prog$, plus 
$\binom{k}{2}$,
we get a fixed bound on the number of shared nodes inside the mod-set.
%
\[
N_{\mathrm{shared}} = k + \binom{k}{2} + \sum_{\proc_i} |\Pvar_{\proc_i}|
\]

$\Pvar^{\proc_i}$ signifies the set of local variables in the procedure $\proc_i$.
Notice that if the same procedure is invoked twice, it has to be included twice
in the sum as well.

\subsection{Verification Condition for the Entire Procedure}
\label{sec:vc:proc}

Since every procedure on its own is loop-free, the verification condition
is straightforward:
%
\[
\vc{\prog} = P_{\prog} \limplies \wlp{\prog}\big(Q_{\prog} \land ``shared\subseteq\Pvar"\big)
\]
%
where $``shared\subseteq\Pvar"$ is a shorthand for the ($\AER$) formula:
%
\bgroup
\renewcommand\a\alpha
\renewcommand\b\beta
\renewcommand\c\gamma
\begin{equation}
\begin{array}{l@{\ }l}
\displaystyle
\forall \a,\b,\c\in&\modset: E_f(\a,\c) \land E_f(\b,\c) \limplies\\
	& \a=\b \lor \bigvee_{v\in\Pvar}\c=v
\end{array}
\end{equation}
\egroup

See \equref{Inversion} for the definition of $E_f$. 
It expresses the obligation mentioned in \secref{working-assumptions} that
all the shared nodes in $\modset$ should be pointed to by local variables,
effectively limiting newly introduced sharing to a bounded number of memory
locations.

\medskip
Now $\vc{\prog}$ is expressed in $\AEAR$, and it is valid if-and-only-if
the program meets its specification. Its validity can be checked using
effectively-propositional logic according to \secref{adaptable-logic}.
