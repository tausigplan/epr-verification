

\section{Adaptable Heap Reachability Logic}
\label{sec:adaptable-logic}

In this section we introduce an extension of $\AER$ from \cite{cav:IBINS13}, 
called \newterm{adaptable heap reachability logic}, and denoted by $\AEAR$.
This extension still has the attractive property of $\AER$,
as it is effectively reducible to the function-free $\forall^*\exists^*$-fragment
of first-order logic, and thus its validity can be checked by a SAT-solver.

\subsection{Preliminaries}

This section reviews the \AFR\ (alternation free) and  \AER\ ($\forall\exists$) logics from \cite{cav:IBINS13}.
They are decidable for validity since their negation corresponds to the BSR fragment \cite{JAR:PiskacMB10}.
These logics include the relation $f^*$ (the reflexive transitive closure of $f$) but 
forbid the explicit use of function symbols including $f$.
Until \secref{extensions} we will use at most one designated backbone function ($f$) per formula.  

\begin{definition}\label{de:AFR}
{\rm
\begin{flushleft}
A \textbf{vocabulary}
$\voc=\B{\constants, \set{f}, \relations}$ is a triple of constant symbols, function
symbol, relation symbols.\\
A \textbf{term}, $t$, is a variable or constant symbol.\\
An \textbf{atomic formula} is one of the following:
\quad(i)~$t_1 = t_2$;\\
\quad(ii)~$r(t_1, t_2, \ldots, t_a)$ where $r$ is a relation symbol of arity $a$;\\
\quad(iii)~$t_1 \abra{f^*} t_2$.\\
\end{flushleft}
A \textbf{quantifier-free formula} ($\QFR$) is a boolean combination of atomic formulas.
A \textbf{universal formula} begins with zero or more universal
quantifiers followed by a quantifier-free formula.
An \textbf{alternation-free formula} ($\AFR$) is a boolean combination of universal formulas.
$\AER$ consists of formulas with quantifier-prefix $\forall^\star \exists^\star$.
}
\end{definition}

In particular, $\QFR \subset \AFR \subset \AER$. 
The preconditions and the postconditions in \figref{UnionFind:code} are all
\AFR\ formulas.


\paragraph{Decidability and Inversion}
\label{sec:decidability-and-inversion}

Every $\AER$ formula can be translated to a first-order  $\forall^{*}\exists^{*}$ formula 
via the following steps~\cite{cav:IBINS13}.
(i)~Add a new uninterpreted relation $R_f$ which is intended to represent $\abra{f^*}$, the
reflexive transitive closure of reachability via $f$, 
(ii)~Add the consistency rule $\Tlinord$ shown in \tabref{Rtnext}, which requires that
$R_f$ is a total order, i.e., reflexive, transitive, acyclic, and linear,
and
(iii)~Replace all occurrences of $t_1 \abra{f^*} t_2$ by $R_f(t_1, t_2)$.

\begin{table*}
\[
\begin{array}{|l|r|}\hline
\forall \alpha: R_f(\alpha,\alpha) \quad \land & \mbox{reflexivity}\\
\forall \alpha, \beta, \gamma: R_f(\alpha, \beta) \land R_f(\beta, \gamma) \to
R_f(\alpha, \gamma)\quad \land & \mbox{transitivity} \\
\forall \alpha,\beta: R_f(\alpha,\beta) \land R_f(\beta,\alpha)\to \alpha = \beta \quad \land & \mbox{acyclicity}\\
\forall \alpha, \beta, \gamma: R_f(\alpha, \beta) \land R_f(\alpha, \gamma) \to  (R_f(\beta, \gamma) \lor R_f(\gamma, \beta)) & \mbox{linearity}\\
\hline
\end{array}
\]
\caption{\label{ta:Rtnext}%
A universal formula $\Tlinord$ requiring that all points reachable from a given
  point are linearly ordered.}
\end{table*}

% The following proposition is proven in \cite[Proposition 3, Appendix A.1]{\trbib}
\begin{proposition}[Simulation of \AER]~\cite[Proposition 3, Appendix A.1]{\trbib}
 \label{pr:simulation}
Consider \AER\ formula $\varphi$ over vocabulary $\voc=\B{\constants, \set{f}, \relations}$. 
Let $\varphi' \eqdef \varphi[R_f(t_1, t_2)/ t_1 \B{f^*} t_2]$.
Then
$\varphi'$ is a first-order  formula over vocabulary $\voc'=\B{\constants, \emptyset, \relations
  \cup \{R_f\}}$ and $\Tlinord\to\varphi'$ is valid if and 
only if the original formula $\varphi$ is valid.
\end{proposition}

When the graph of $f$ is acyclic, the relation $E_f$ characterizing the function $f$ can be
recovered from its reflexive transitive closure, $f^*$, at the cost of an extra 
universal quantifier:
\begin{equation}
E_f(\alpha, \beta) ~\eqdef~ \alpha \B{f^+} \beta  \land \forall \gamma: \alpha \B{f^+} \gamma \implies \beta \B{f^*} \gamma
 \label{eq:Inversion}
 \end{equation}
Here $\alpha \B{f^+} \beta ~\eqdef~ \alpha \B{f^*} \beta \land \alpha \neq \beta$.


\subsection{Adaptable Heap Reachability Logic}

The new logic $\AEAR$ is obtained by augmenting $\AER$ with unary function symbols, denoted by $g,h_1 \til h_n$ where:
\begin{itemize}
\item $g$ must be interpreted as an idempotent function.
\item The images $h_1 \til h_n$ are all bounded by some pre-determined parameter $N$, that is: each $h_i$ takes at most $N$ distinct values.
\item Function symbols may not be nested, i.e., all terms involving function symbols have the form
  $f(z)$, where $z$ is a variable. 
\end{itemize}

We later show that $\AEAR$ suffices for expressing the verification conditions of the programs discussed above. In the typical use case, the function $g$ assigns the entry point in the mod-set
for every node (called $\en{\modp}$ above), and the functions $h_1 \til h_n$
are used for expressing the entry points in inner mod-sets.
The main attractive feature of this logic is given in the following theorem.

\begin{theorem}
\label{th:AEAR-translation}
Any $\AEAR$-formula $\varphi$ can be translated to an equi-valid (first-order)
function-free $\forall^*\exists^*$-formula.
\end{theorem}

The proof of \theref{AEAR-translation}, given in~\logicalannex, begins by translating 
$\varphi$ to a $\forall^*\exists^*$-formula $\varphi'$  as described in \prref{simulation}, without
modifying the function symbols $g,h_1\til h_n$.  The function symbols are then replaced by
new relation and constant symbols.  We add new universal formulas to express 
the above semantic restrictions on the functions.
